import { Injectable } from '@angular/core';

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [
  
  
  // datatable
  { name: 'c1', src: 'assets/app-assets/js/scripts/ui/data-list-view.min.js' },
  { name: 'c2', src: 'assets/app-assets/js/scripts/datatables/datatable.min.js' },
  { name: 's1', src: 'assets/js/main-steps.js' },
  { name: 's2', src: 'assets/js/contrat.min.js' },
  { name: 's3', src: 'assets/app-assets/js/scripts/extensions/fullcalendar.min.js' },
  { name: 's4', src: 'assets/app-assets/js/scripts/app-invoice.min.js' },
  { name: 's5', src: 'assets/app-assets/css/materialize.min.css' },
  { name: 's6', src: 'assets/app-assets/css/style.min.css' },
  { name: 's7', src: 'http://www.carqueryapi.com/js/carquery.0.3.4.js' },
  { name: 's8', src: 'assets/app-assets/js/cars.min.js' },
  { name: 'images', src: 'assets/app-assets/images/Dropbox/Assurance/*.*' },

  ];
declare var document: any;

@Injectable()
export class DynamicFileAdminServiceService {

  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
    // 
   
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
    return new Promise((resolve, reject) => {
      if (!this.scripts[name].loaded) {
        //load script
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
       // debut code xxxxxx 
       // cet code permet d'importer les fichiers pour chaque access au page  
        script.onreadystatechange = () => {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                this.scripts[name].loaded = true;
                resolve({script: name, loaded: true, status: 'Loaded'});
            }
        };
        // fin xxxxxxxxxxxxxxx
        console.log(name);
        script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('body')[0].appendChild(script);
      } else {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
    });
  }

  // 


  

}









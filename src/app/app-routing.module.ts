import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreationDevisComponent } from './espace-admin/creation-devis/creation-devis.component';
import { CreationGarantieComponent } from './espace-admin/creation-garantie/creation-garantie.component';
import { ListeDevisComponent } from './espace-admin/liste-devis/liste-devis.component';
import { CreationRisqueComponent } from './espace-admin/creation-risque/creation-risque.component';
import { CreationFamilleProduitComponent } from './espace-admin/creation-famille-produit/creation-famille-produit.component';
import { CreationConfirationDevisguComponent } from './espace-admin/creation-confiration-devisgu/creation-confiration-devisgu.component';
import { ListeRisqueComponent } from './espace-admin/liste-risque/liste-risque.component';
import { ListeGarantieComponent } from './espace-admin/liste-garantie/liste-garantie.component';
import { ListeFamilleProduitComponent } from './espace-admin/liste-famille-produit/liste-famille-produit.component';
import { ListeConfigurationDevisComponent } from './espace-admin/liste-configuration-devis/liste-configuration-devis.component';
import { CreationContratComponent } from './espace-admin/creation-contrat/creation-contrat.component';
import { ListeContratsComponent } from './espace-admin/liste-contrats/liste-contrats.component';
import { CreationQuittanceComponent } from './espace-admin/creation-quittance/creation-quittance.component';
import { CreationCompagnieComponent } from './espace-admin/creation-compagnie/creation-compagnie.component';
import { ListeQuittancesComponent } from './espace-admin/liste-quittances/liste-quittances.component';
import { ListeCompagniesComponent } from './espace-admin/liste-compagnies/liste-compagnies.component';
import { ListeUtilisateurComponent } from './espace-admin/liste-utilisateur/liste-utilisateur.component';
import { CreationUtilisateurComponent } from './espace-admin/creation-utilisateur/creation-utilisateur.component';
import { ListeAgenceComponent } from './espace-admin/liste-agence/liste-agence.component';
import { CreationAgenceComponent } from './espace-admin/creation-agence/creation-agence.component';
import { CreationSinistresComponent } from './espace-admin/creation-sinistres/creation-sinistres.component';
import { CreationClientComponent } from './espace-admin/creation-client/creation-client.component';
import { ListeSinistresComponent } from './espace-admin/liste-sinistres/liste-sinistres.component';
import { ListeClientsComponent } from './espace-admin/liste-clients/liste-clients.component';
import { CreationApporteurComponent } from './espace-admin/creation-apporteur/creation-apporteur.component';
import { ListeApporteursComponent } from './espace-admin/liste-apporteurs/liste-apporteurs.component';
import { CreateReclamationComponent } from './espace-admin/create-reclamation/create-reclamation.component';
import { ListeReclamationsComponent } from './espace-admin/liste-reclamations/liste-reclamations.component';
import { CreateReglementComponent } from './espace-admin/create-reglement/create-reglement.component';
import { ListeReglementsComponent } from './espace-admin/liste-reglements/liste-reglements.component';
import { CreateBordereauComponent } from './espace-admin/create-bordereau/create-bordereau.component';
import { ListeBordereauxComponent } from './espace-admin/liste-bordereaux/liste-bordereaux.component';
import { ConnexionComponent } from './espace-admin/connexion/connexion.component';
import { DevisComponent } from './espace-admin/devis/devis.component';
import { ContratComponent } from './espace-admin/contrat/contrat.component';
import { ComptabiliteComponent } from './espace-admin/comptabilite/comptabilite.component';
import { SinistreComponent } from './espace-admin/sinistre/sinistre.component';
import { ConfigurationComponent } from './espace-admin/configuration/configuration.component';
import { GedComponent } from './espace-admin/ged/ged.component';
import { ProfilComponent } from './espace-admin/profil/profil.component';
import { ContentComponent } from './espace-admin/content/content.component';
import { ListeSouhaitComponent } from './espace-admin/liste-souhait/liste-souhait.component';
import { MenuComponent } from './espace-admin/menu/menu.component';
import { ForgotPasswordComponent } from './espace-admin/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './espace-admin/reset-password/reset-password.component';
import { CreationRendezVousComponent } from './espace-admin/creation-rendez-vous/creation-rendez-vous.component';
import { ListeRendezVousComponent } from './espace-admin/liste-rendez-vous/liste-rendez-vous.component';
import { ListeDocumentNonTraiteComponent } from './espace-admin/liste-document-non-traite/liste-document-non-traite.component';
import { ListeDocumentTraiteComponent } from './espace-admin/liste-document-traite/liste-document-traite.component';
import { ModifierContratComponent } from './espace-admin/modifier-contrat/modifier-contrat.component';
import { CreationAvenantComponent } from './espace-admin/creation-avenant/creation-avenant.component';
import { ListeAvenantComponent } from './espace-admin/liste-avenant/liste-avenant.component';
import { UtilisateurService } from './services/utilisateur.service';
import { CanActivateService } from './services/can-activate.service';
import { TokenStorageService } from './services/token-storage.service';
import { AdminService } from './services/admin.service';
import { AgenceService } from './services/agence.service';


const routes: Routes = [{ path:'creation_devis',component:CreationDevisComponent,canActivate:[UtilisateurService] },
{ path:'creation_garantie',component:CreationGarantieComponent ,canActivate:[UtilisateurService]},
{ path:'liste_devis',component:ListeDevisComponent,canActivate:[UtilisateurService]},
{ path:'creation_risque',component:CreationRisqueComponent ,canActivate:[UtilisateurService]},
{ path:'creation_familleproduit',component:CreationFamilleProduitComponent,canActivate:[UtilisateurService]},
{ path:'creation_configurationdevis',component:CreationConfirationDevisguComponent,canActivate:[UtilisateurService]},
{ path:'liste_risques',component:ListeRisqueComponent,canActivate:[UtilisateurService]},
{ path:'liste_garanties',component:ListeGarantieComponent,canActivate:[UtilisateurService]},
{ path:'liste_familleproduits',component:ListeFamilleProduitComponent,canActivate:[UtilisateurService]},
{ path:'liste_configurationdevis',component:ListeConfigurationDevisComponent,canActivate:[UtilisateurService]},
{ path:'creation_contrat',component:CreationContratComponent,canActivate:[UtilisateurService]},
{ path:'liste_contrats',component:ListeContratsComponent,canActivate:[UtilisateurService]},
{ path:'creation_quittance',component:CreationQuittanceComponent,canActivate:[UtilisateurService]},
{ path:'creation_compagnie',component:CreationCompagnieComponent,canActivate:[AgenceService]},
{ path:'liste_quittances',component:ListeQuittancesComponent,canActivate:[UtilisateurService]},
{ path:'liste_compagnies',component:ListeCompagniesComponent,canActivate:[AgenceService]},
{ path:'liste_utilisateur',component:ListeUtilisateurComponent,canActivate:[AgenceService]},
{ path:'creation_utilisateur',component:CreationUtilisateurComponent,canActivate:[AgenceService]},
{ path:'liste_agence',component:ListeAgenceComponent,canActivate:[AdminService]},
{ path:'creation_agence',component:CreationAgenceComponent,canActivate:[AdminService]},
{ path:'creation_sinistres',component:CreationSinistresComponent,canActivate:[UtilisateurService]},
{ path:'creation_client',component:CreationClientComponent,canActivate:[UtilisateurService]},
{ path:'liste_sinistres',component:ListeSinistresComponent,canActivate:[UtilisateurService]},
{ path:'liste_clients',component:ListeClientsComponent,canActivate:[UtilisateurService]},
{ path:'creation_apporteur',component:CreationApporteurComponent,canActivate:[UtilisateurService]},
{ path:'liste_apporteurs',component:ListeApporteursComponent,canActivate:[UtilisateurService]},
{ path:'creation_reclamation',component:CreateReclamationComponent,canActivate:[UtilisateurService]},
{ path:'liste_reclamations',component:ListeReclamationsComponent,canActivate:[UtilisateurService]},
{ path:'create_reglement',component:CreateReglementComponent,canActivate:[UtilisateurService]},
{ path:'liste_reglements',component:ListeReglementsComponent,canActivate:[UtilisateurService]},
{ path:'create_bordereau',component:CreateBordereauComponent,canActivate:[UtilisateurService]},
{ path:'liste_bordereaux',component:ListeBordereauxComponent,canActivate:[UtilisateurService]},
{ path:'accueil',component:ConnexionComponent, canActivate:[TokenStorageService]},
{ path:'devis',component:DevisComponent,canActivate:[UtilisateurService]},
{ path:'contrat',component:ContratComponent,canActivate:[UtilisateurService]},
{ path:'comptabilite',component:ComptabiliteComponent, canActivate:[UtilisateurService]},
{ path:'sinistre',component:SinistreComponent, canActivate:[UtilisateurService]},
{ path:'configuration',component:ConfigurationComponent, canActivate:[TokenStorageService]},
{ path:'ged',component:GedComponent,canActivate:[UtilisateurService]},
{ path:'profil',component:ProfilComponent,canActivate:[TokenStorageService]},
{ path:'',component:ContentComponent,canActivate:[CanActivateService]},
{ path:'liste_souhait',component:ListeSouhaitComponent,canActivate:[UtilisateurService]},
{ path:'menu',component:MenuComponent},
{ path:'forgot_password',component:ForgotPasswordComponent},
{ path:'reset_password',component:ResetPasswordComponent},
{ path:'creation_rendez-vous',component:CreationRendezVousComponent},
{ path:'liste_rendez-vous',component:ListeRendezVousComponent},
{ path:'liste_document_non_traite',component:ListeDocumentNonTraiteComponent},
{ path:'liste_document_traite',component:ListeDocumentTraiteComponent},
{ path:'modifierContrat/:id',component:ModifierContratComponent},
{ path:'creation_avenant',component:CreationAvenantComponent,canActivate:[UtilisateurService]},
{ path:'liste_avenants',component:ListeAvenantComponent,canActivate:[UtilisateurService]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import { Router }  from "@angular/router";
import { TokenStorageService } from './services/token-storage.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';
  constructor(public router: Router ,private tokenStorage: TokenStorageService){}
  token:string=this.tokenStorage.getToken();

}

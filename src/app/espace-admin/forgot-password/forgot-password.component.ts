import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private service:UserService,private router:Router) { }
utilisateur:User;
emailExist;
  ngOnInit(): void {
  }
  forgetPassword(utilisateur){
    this.service.getCode(utilisateur.email).subscribe((data)=>{
      console.log(data["message"]);
      if(data["message"])
        this.emailExist=false;
        else{
          this.emailExist=true;
          this.router.navigate(['/reset_password/'], { queryParams: {email: utilisateur.email}});

        }
    })
    console.log(this.emailExist);

  }

}

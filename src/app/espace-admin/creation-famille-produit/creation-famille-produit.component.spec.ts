import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationFamilleProduitComponent } from './creation-famille-produit.component';

describe('CreationFamilleProduitComponent', () => {
  let component: CreationFamilleProduitComponent;
  let fixture: ComponentFixture<CreationFamilleProduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationFamilleProduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationFamilleProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

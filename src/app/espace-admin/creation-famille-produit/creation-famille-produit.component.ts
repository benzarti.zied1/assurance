import { Component, OnInit } from '@angular/core';
import { RisqueService } from 'src/app/services/risque.service';
import { Risque } from 'src/app/interfaces/risque';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import swal from 'sweetalert2';
import { HttpClient, HttpHeaders,HttpClientModule,HttpEventType } from '@angular/common/http';



@Component({
  selector: 'app-creation-famille-produit',
  templateUrl: './creation-famille-produit.component.html',
  styleUrls: ['./creation-famille-produit.component.css']
})
export class CreationFamilleProduitComponent implements OnInit {

  constructor(private http: HttpClient,private risqueService:RisqueService,private service: FamilleProduitService ) { }
  selectedFile : File = null;

  famille:FamilleProduit= {
    image: '',
    nom: '',
    risque:'Choisir un Risque'
  };
  risque:Risque[]=[];
  InvalidFrom;
  ngOnInit(): void {

    this.getAllRisque();
  }
  public getAllRisque(){
    return this.risqueService.search().subscribe(data=>{
     
    this.risque= data["risque"].map(el=>{
       return {
         id:el.id,
         code:el.code,
         nom:el.nom
        
        }
     })   

    })
  }

  save(familleproduit:FamilleProduit) {
    this.onUpload();
    familleproduit.image=this.selectedFile.name;
    this.service.add(familleproduit)
     .subscribe(data =>{ //console.log(data);
      this.succes();
    
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 

    });
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau Famille Produit!',
      'success'
    )

  }
  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
  }
  
  onUpload(){
    const fd = new FormData();
    fd.append('image', this.selectedFile, this.selectedFile.name);
   // console.log(fd);
    //console.log(this.selectedFile.name);
    this.http.post('http://localhost:3000/single', fd)
      .subscribe(res => {
        console.log(res);
      });
    
    
  
  }
}

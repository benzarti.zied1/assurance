import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services/user.service';
import {User} from '../../interfaces/user';
import { Router } from '@angular/router';
import { TokenStorageService } from '../../services/token-storage.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {


  constructor(private service:UserService,private router: Router,private tokenStorage: TokenStorageService)
   { }

  ngOnInit(): void {

  
  }
  
  save() {
    
    let data = document.getElementById('invoice-template');  
     html2canvas(data).then(canvas => {
      
      var pdf=new jspdf("p", "mm", "a4");
      let width = pdf.internal.pageSize.getWidth()
      let height = pdf.internal.pageSize.getHeight()
      
      let widthRatio = width / canvas.width
      let heightRatio = height / canvas.height
      
      let ratio = widthRatio > heightRatio ? heightRatio : widthRatio
      
      pdf.addImage(
        canvas.toDataURL('image/jpeg', 1.0),
        'JPEG',
        0,
        0,
        canvas.width * ratio,
        canvas.height * ratio,
      )
                    pdf.save('test11.pdf');
     }); 

}
}
import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';


@Component({
  selector: 'app-devis',
  templateUrl: './devis.component.html',
  styleUrls: ['./devis.component.css']
})
export class DevisComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit() {
    
    this.loadScripts();
 }

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }



}

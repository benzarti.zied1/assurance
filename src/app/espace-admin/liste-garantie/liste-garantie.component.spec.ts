import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeGarantieComponent } from './liste-garantie.component';

describe('ListeGarantieComponent', () => {
  let component: ListeGarantieComponent;
  let fixture: ComponentFixture<ListeGarantieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeGarantieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeGarantieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

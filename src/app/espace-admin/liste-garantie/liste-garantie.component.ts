import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { Garantie } from 'src/app/interfaces/garantie';
import { GarantieService } from 'src/app/services/garantie.service';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-liste-garantie',
  templateUrl: './liste-garantie.component.html',
  styleUrls: ['./liste-garantie.component.css']
})
export class ListeGarantieComponent implements OnInit {

  famille:any[]=[];
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService, private service:GarantieService,private familleService:FamilleProduitService) { }
garantie:any[]=[];
InvalidFrom;

  ngOnInit() {
    
     this.loadScripts();
     this.getAll();
    this.getAllFamille();
  }

  public getAllFamille(){
    return this.familleService.search().subscribe(data=>{
      this.famille= data["familleproduit"];   
  
      })
    
  }
  public getAll(){
   
    return this.service.search().subscribe(data=>{  
      this.garantie= data["garantie"].map(el=>{   
    
        return {
         id:el.id,
         nom:el.nom,
         niveau:el.niveau,
         montantAssure:el.montantAssure,
         montantFranchise:el.montantFranchise,
         familleproduit:el.familleproduitId   
        }
     })
  
console.log(this.garantie);
    })
  }

  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }

  deletePopup(id,nom){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer le garantie "+nom,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer le garantie avec succes.',
          'success'
        ).then(()=>{
          this.delete(id);
        })
      }
    })
  }

  update(f){
    this.service.update(f)
    .subscribe(data =>{ 
     this.succes();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }
  succes(): void{
    swal.fire(
      'Modification!',
      'Votre avez modifier votre Garantie!',
      'success'
    )
    
    } 
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }

}

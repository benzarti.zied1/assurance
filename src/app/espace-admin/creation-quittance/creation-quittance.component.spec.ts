import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationQuittanceComponent } from './creation-quittance.component';

describe('CreationQuittanceComponent', () => {
  let component: CreationQuittanceComponent;
  let fixture: ComponentFixture<CreationQuittanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationQuittanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationQuittanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

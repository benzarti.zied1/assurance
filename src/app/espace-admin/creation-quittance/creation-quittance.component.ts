import { Component, OnInit } from '@angular/core';
import { ContratService } from 'src/app/services/contrat.service';
import swal from 'sweetalert2';
import { QuittanceService } from 'src/app/services/quittance.service';


@Component({
  selector: 'app-creation-quittance',
  templateUrl: './creation-quittance.component.html',
  styleUrls: ['./creation-quittance.component.css']
})
export class CreationQuittanceComponent implements OnInit {
  selectedValueQuittance;
  selectedValueContrat;
  contrats:any[]=[];
  InvalidFrom;
  cht:Array<number>=[];
  taxe:Array<number>=[];
  cttc:Array<number>=[];
  counter = Array;
  prixht=0;
  prixtaxe=0;
  prixttc=0;
  constructor(private serviceContrat:ContratService,private service:QuittanceService) { }

  ngOnInit(): void {
    this.getAll();
  }
  public getAll(){
   
    return this.serviceContrat.search().subscribe(data=>{  
      this.contrats= data
    });
    
  }

  public createQuittance(f){
    f.totalHt=this.prixht;
    f.totalTaxe=this.prixtaxe;
    f.total=this.prixttc;

    return this.service.createQuittance(f).subscribe(data =>{ console.log(data);
      this.succes();
   },error=>{
     this.InvalidFrom=error["error"]["message"]; 
   
   })
  }
  
  succes(): void{
    swal.fire(
      'Creation Quittance!',
      'Votre avez creer un nouveau quittance!',
      'success'
    ).then(()=>{
      location.reload();
    })
    
    }

    calculerttc(){
      for(var i =0 ; i<this.taxe.length;i++){
        var prixttc=0;
        prixttc=this.cht[i]+this.taxe[i];
        this.cttc[i]=prixttc;
      }
      this.calculerprix();
    }

    calculerprix(){
      this.prixtaxe=0;
      this.prixht=0;
      this.prixttc=0;
      for(var i =0 ; i<this.taxe.length;i++){
        this.prixtaxe=this.prixtaxe+this.taxe[i];
        this.prixht+=this.cht[i];
        
      }
      this.prixttc=this.prixtaxe+this.prixht;
    }
}

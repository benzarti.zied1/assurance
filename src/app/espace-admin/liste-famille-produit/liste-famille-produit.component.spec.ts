import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeFamilleProduitComponent } from './liste-famille-produit.component';

describe('ListeFamilleProduitComponent', () => {
  let component: ListeFamilleProduitComponent;
  let fixture: ComponentFixture<ListeFamilleProduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeFamilleProduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeFamilleProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

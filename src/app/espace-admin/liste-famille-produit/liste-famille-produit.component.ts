import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { RisqueService } from 'src/app/services/risque.service';
import swal from 'sweetalert2';
import { HttpClient, HttpHeaders,HttpClientModule,HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-liste-famille-produit',
  templateUrl: './liste-famille-produit.component.html',
  styleUrls: ['./liste-famille-produit.component.css']
})
export class ListeFamilleProduitComponent implements OnInit {
  selectedFile : File = null;

  risque:any[]=[];
  constructor(private http: HttpClient,private dynamicScriptLoader: DynamicFileAdminServiceService, private service:FamilleProduitService,private risqueService:RisqueService) { }
famille:FamilleProduit[]=[];
InvalidFrom;

  ngOnInit() {
    
     this.loadScripts();
     this.getAll();
    this.getAllRisque();
  }

  public getAllRisque(){
    return this.risqueService.search().subscribe(data=>{
      this.risque= data["risque"];   
  
      })
    
  }
  public getAll(){
   
    return this.service.search().subscribe(data=>{  
      this.famille= data["familleproduit"].map(el=>{   
    
      console.log("el.risquye == "+el.risqueId);
        return {
         id:el.id,
         image:el.image,
         nom:el.nom,
         risque:el.risqueId   
        }
     })
  

    })
  }

  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }

  deletePopup(id,nom){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer le famille Produit "+nom,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer le Famille Produit avec succes.',
          'success'
        ).then(()=>{
          this.delete(id);
        })
      }
    })
  }

  update(f){
    this.onUpload();
    f.image=this.selectedFile.name;
    this.service.update(f)
    .subscribe(data =>{ 
     this.succes();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
  }
  onUpload(){
    const fd = new FormData();
    fd.append('image', this.selectedFile, this.selectedFile.name);
   // console.log(fd);
    //console.log(this.selectedFile.name);
    this.http.post('http://localhost:3000/single', fd)
      .subscribe(res => {
        console.log(res);
      });
    
    //this.updateAgence(this.agence);
  
  }
  succes(): void{
    swal.fire(
      'Modification!',
      'Votre avez modifier votre famille Produit!',
      'success'
    )
    
    }  
 
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }

}

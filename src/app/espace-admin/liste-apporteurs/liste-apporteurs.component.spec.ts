import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeApporteursComponent } from './liste-apporteurs.component';

describe('ListeApporteursComponent', () => {
  let component: ListeApporteursComponent;
  let fixture: ComponentFixture<ListeApporteursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeApporteursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeApporteursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

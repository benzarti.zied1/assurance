import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Societe } from 'src/app/interfaces/societe';
import { Particulier } from 'src/app/interfaces/particulier';
import swal from 'sweetalert2';
import { ApporteurService } from 'src/app/services/apporteur.service';


@Component({
  selector: 'app-liste-apporteurs',
  templateUrl: './liste-apporteurs.component.html',
  styleUrls: ['./liste-apporteurs.component.css']
})
export class ListeApporteursComponent implements OnInit {
  InvalidFromParticulier;
  InvalidFromSociete;
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:ApporteurService) { }
  a:any={
    id:'',
    nom:'',
    email:'',
    tel:'',
    mobile:'',
    adresse:'',
    cin:''
  }

  s:any={
    id:'',
    nom:'',
    email:'',
    tel:'',
    mobile:'',
    adresse:'',
    raison_sociale:'',
    fax:'',
    numero_registre:'',
    date_creation:'',
    siteWeb:''
  }
 apporteur:any[]=[];
particulier;
societe;
c;
p;
su:Societe[]=[];
pu:Particulier[]=[];
particuliers:Particulier[]=[];
societes:Societe[]=[];

  ngOnInit() {
    
     this.loadScripts();
    // this.getAll();
     this.getAllParticulier();
     this.getAllSociete();

   

  }
  /*
  public getAll(){
    return this.service.search().subscribe(data=>{
     
    this.client= data["client"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })
 
 

    })

  

  }
*/
  public getAllParticulier(){
    
    return this.service.searchParticulier().subscribe(data=>{
     
      this.apporteur= data["apporteur"].map(el=>{
         return {
           id:el.id,
           nom:el.nom,
           email:el.email,
           adresse:el.adresse,
           tel:el.tel,
           mobile:el.mobile
          
          }
          
       })

       this.particulier= data["apporteurparticulier"].map(el=>{
        return {
          id:el.id,
          cin:el.cin
         
         }
         
      })



      for(let i=0;i<this.apporteur.length;i++){
        this.a={
          id:this.apporteur[i].id,
          nom:this.apporteur[i].nom,
          email:this.apporteur[i].email,
          tel:this.apporteur[i].tel,
          mobile:this.apporteur[i].mobile,
          adresse:this.apporteur[i].adresse,
          cin:this.particulier[i].cin
        }
        this.particuliers.push(this.a);
        
            }
   
   
  
      })
  

  }


  public getAllSociete(){
    
    return this.service.searchSociete().subscribe(data=>{
     
      this.apporteur= data["apporteur"].map(el=>{
         return {
           id:el.id,
           nom:el.nom,
           email:el.email,
           adresse:el.adresse,
           tel:el.tel,
           mobile:el.mobile
          
          }
          
       })

       this.societe= data["apporteursociete"].map(el=>{
        return {
          id:el.id,
          raison_sociale:el.raison_sociale,
          fax:el.fax,
          numero_registre:el.numero_registre,
          date_creation:el.date_creation,
          siteWeb:el.siteWeb
         }
         
      })

      

      for(let i=0;i<this.apporteur.length;i++){
        this.s={
          id:this.apporteur[i].id,
          nom:this.apporteur[i].nom,
          email:this.apporteur[i].email,
          tel:this.apporteur[i].tel,
          mobile:this.apporteur[i].mobile,
          adresse:this.apporteur[i].adresse,
          raison_sociale:this.societe[i].raison_sociale,
          fax:this.societe[i].fax,
          numero_registre:this.societe[i].numero_registre,
          date_creation:this.societe[i].date_creation,
          siteWeb:this.societe[i].siteWeb
        }
        this.societes.push(this.s);
        
            }
   
   
  
      })
  

  }
/*
public check(id){
  return this.service.checkClient(id).subscribe(data=>{
    this.c=data;
  })
}*/


updateParticulier(particulier){
  this.service.updateParticulier(particulier)
  .subscribe(data =>{ console.log(data);
   this.succes();
},error=>{
  this.InvalidFromParticulier=error["error"]["message"]; 

})
}

updateSociete(societe){
  this.service.updateSociete(societe)
  .subscribe(data =>{ console.log(data);
   this.succes();
},error=>{
  this.InvalidFromSociete=error["error"]["message"]; 

})
}

deleteParticulier(id){
  return this.service.deleteParticulier(id).subscribe(data=>{
    location.reload();
  })
}

deleteSociete(id){
  return this.service.deleteSociete(id).subscribe(data=>{
    location.reload();
  })
}

succes(): void{
swal.fire(
  'Modification!',
  'Votre avez modifier votre apporteur!',
  'success'
)

}

deletePopupParticulier(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer apporteur "+nom+id,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer apporteur avec succes.',
        'success'
      ).then(()=>{
        this.deleteParticulier(id);
      })
    }
  })
}


deletePopupSociete(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer apporteur "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer apporteur avec succes.',
        'success'
      ).then(()=>{
        this.deleteSociete(id);
      })
    }
  })
}
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }



}

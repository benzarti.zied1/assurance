import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationConfirationDevisguComponent } from './creation-confiration-devisgu.component';

describe('CreationConfirationDevisguComponent', () => {
  let component: CreationConfirationDevisguComponent;
  let fixture: ComponentFixture<CreationConfirationDevisguComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationConfirationDevisguComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationConfirationDevisguComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

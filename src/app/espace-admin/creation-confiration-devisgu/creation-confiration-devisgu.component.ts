import { Component, OnInit } from '@angular/core';
import { ConfigurationDevis } from 'src/app/interfaces/configuration-devis';
import { Risque } from 'src/app/interfaces/risque';
import { RisqueService } from 'src/app/services/risque.service';
import { ConfigurationDevisService } from 'src/app/services/configuration-devis.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-creation-confiration-devisgu',
  templateUrl: './creation-confiration-devisgu.component.html',
  styleUrls: ['./creation-confiration-devisgu.component.css']
})
export class CreationConfirationDevisguComponent implements OnInit {
  configuration:ConfigurationDevis= {
    code: '',
    questionnaire: '',
    risque:'Choisir un Risque'
  };
  risque:Risque[]=[];
  InvalidFrom;
  constructor(private risqueService:RisqueService,private service: ConfigurationDevisService) { }

  ngOnInit(): void {
    this.getAllRisque();

  }
  public getAllRisque(){
    return this.risqueService.search().subscribe(data=>{
     
    this.risque= data["risque"].map(el=>{
       return {
         id:el.id,
         code:el.code,
         nom:el.nom
        
        }
     })   

    })
  }

  save(configuration:ConfigurationDevis) {
    
    this.service.add(configuration)
     .subscribe(data =>{ //console.log(data);
      this.succes();
    
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 

    });
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau Configuration!',
      'success'
    )

  }

}

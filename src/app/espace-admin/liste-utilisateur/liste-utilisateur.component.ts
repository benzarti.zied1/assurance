import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { Utilisateur } from 'src/app/interfaces/utilisateur';
import { User } from 'src/app/interfaces/user';
import swal from 'sweetalert2';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-liste-utilisateur',
  templateUrl: './liste-utilisateur.component.html',
  styleUrls: ['./liste-utilisateur.component.css']
})
export class ListeUtilisateurComponent implements OnInit {
  InvalidFrom;
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:UtilisateurService) { }
user:User[]=[];
utilisateur:Utilisateur[]=[];
utilisateurs:Utilisateur[]=[];

u:any={      
  id:'',
  username:'',
  email:'',
  password:'',
  nom:'',
  prenom:'',
  cin:'',
  date_naissance:new Date(),
  ville:'',
  codePostal:'',
  role:'',
  tel:'',
  autreTel:'',
 }


//utilisateur=[];

  ngOnInit() {
    
     this.loadScripts();
     this.getAll();

  }

  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }
  public getAll(){
    return this.service.search().subscribe(data=>{
     
    this.user= data["user"].map(el=>{
       return {
         id:el.id,
         username:el.username,
         email:el.email,
         password:el.password
        
        }
     })

     this.utilisateur= data["utilisateur"].map(el=>{
      return {
        id:el.id,
        nom:el.nom,
        prenom:el.prenom,
        cin:el.cin,
        date_naissance:el.date_naissance,
        ville:el.ville,
        codePostal:el.codePostal,
        role:el.role,
        tel:el.tel,
        autreTel:el.autreTel,
        Userid:el.Userid
       
       }
    })
    for(let i=0;i<this.utilisateur.length;i++){
this.u={
  id:this.user[i].id,
  username:this.user[i].username,
  email:this.user[i].email,
  password:this.user[i].password,
  nom:this.utilisateur[i].nom,
  prenom:this.utilisateur[i].prenom,
  cin:this.utilisateur[i].cin,
  date_naissance:this.utilisateur[i].date_naissance,
  ville:this.utilisateur[i].ville,
  codePostal:this.utilisateur[i].codePostal,
  role:this.utilisateur[i].role,
  tel:this.utilisateur[i].tel,
  autreTel:this.utilisateur[i].autreTel,
}
this.utilisateurs.push(this.u);

    }
     

    })
  }

  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }
  update(u){
    this.service.update(u)
    .subscribe(data =>{ console.log(data);
     this.succes();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }
succes(): void{
  swal.fire(
    'Modification!',
    'Vous avez modifier votre utilisateur!',
    'success'
  )

}
deletePopup(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer utilisateur "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer utilisateur avec succes.',
        'success'
      ).then(()=>{
        this.delete(id);
      })
    }
  })
}



}

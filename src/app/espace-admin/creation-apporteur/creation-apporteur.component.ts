import { Component, OnInit } from '@angular/core';
import { Particulier } from 'src/app/interfaces/particulier';
import swal from 'sweetalert2';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Societe } from 'src/app/interfaces/societe';
import { SocieteService } from 'src/app/services/societe.service';
import { ApporteurService } from 'src/app/services/apporteur.service';

@Component({
  selector: 'app-creation-apporteur',
  templateUrl: './creation-apporteur.component.html',
  styleUrls: ['./creation-apporteur.component.css']
})
export class CreationApporteurComponent implements OnInit {

  InvalidFromParticulier;
  InvalidFromSociete;
  particulier : any = {
    nomP:'',
    cin:'', 
    adresseP:'',
    emailP:'',
    telP:'',
    mobileP:''
  };

  societe : Societe = {
    nom:'',
    adresse:'',
    email:'',
    tel:'',
    mobile:'',
    raison_sociale:'',
    numero_registre:'', 
    date_creation:null,
    siteWeb:'',
    fax:'',
  };
  constructor(private service:ApporteurService,private serviceSociete:SocieteService) { }

  ngOnInit(): void {
  }

  saveParticulier(particulier:Particulier) {
    
  
    this.service.addParticulier(particulier)
     .subscribe(data =>{ console.log(data);
      this.succes();
     
        },
     
     error => {//console.log(error);
      this.InvalidFromParticulier=error["error"]["message"]; 

    });
  }

  saveSociete(societe:Societe) {
    
  
    this.service.addSociete(societe)
     .subscribe(data =>{ console.log(data);
      this.succes();
     
        },
     
     error => {//console.log(error);
      this.InvalidFromSociete=error["error"]["message"]; 

    });
    console.log(societe);
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau apporteur!',
      'success'
    )

  }

}

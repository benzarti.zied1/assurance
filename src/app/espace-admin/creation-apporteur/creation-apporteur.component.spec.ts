import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationApporteurComponent } from './creation-apporteur.component';

describe('CreationApporteurComponent', () => {
  let component: CreationApporteurComponent;
  let fixture: ComponentFixture<CreationApporteurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationApporteurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationApporteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

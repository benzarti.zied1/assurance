import { Component, OnInit } from '@angular/core';
import { Garantie } from 'src/app/interfaces/garantie';
import { GarantieService } from 'src/app/services/garantie.service';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import swal from 'sweetalert2';

@Component({
  selector: 'app-creation-garantie',
  templateUrl: './creation-garantie.component.html',
  styleUrls: ['./creation-garantie.component.css']
})
export class CreationGarantieComponent implements OnInit {
  garantie:Garantie= {
    nom: '',
    familleproduit:'Choisir un Famille',
    niveau:'',
    montantFranchise:'',
    montantAssure:''
  };
  InvalidFrom;
  constructor(private familleService:FamilleProduitService,private service: GarantieService) { }

  ngOnInit(): void {
    this.getAllFamille();
  }
  famille:FamilleProduit[]=[];


  public getAllFamille(){
    return this.familleService.search().subscribe(data=>{
     
    this.famille= data["familleproduit"].map(el=>{
       return {
         id:el.id,
         nom:el.nom
        }
     })   

    })
  }

  save(garantie:Garantie) {
    
    this.service.add(garantie)
     .subscribe(data =>{ //console.log(data);
      this.succes();
    
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 

    });
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau Garantie!',
      'success'
    )

  }

}

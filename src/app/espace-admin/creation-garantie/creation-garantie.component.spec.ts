import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationGarantieComponent } from './creation-garantie.component';

describe('CreationGarantieComponent', () => {
  let component: CreationGarantieComponent;
  let fixture: ComponentFixture<CreationGarantieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationGarantieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationGarantieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

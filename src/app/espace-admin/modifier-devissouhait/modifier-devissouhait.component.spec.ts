import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierDevissouhaitComponent } from './modifier-devissouhait.component';

describe('ModifierDevissouhaitComponent', () => {
  let component: ModifierDevissouhaitComponent;
  let fixture: ComponentFixture<ModifierDevissouhaitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierDevissouhaitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierDevissouhaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

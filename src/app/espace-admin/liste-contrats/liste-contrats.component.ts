import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { ContratService } from 'src/app/services/contrat.service';
import { ParticulierService } from 'src/app/services/particulier.service';
import { RisqueService } from 'src/app/services/risque.service';
import { GarantieService } from 'src/app/services/garantie.service';
import { Risque } from 'src/app/interfaces/risque';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import { Voiture } from 'src/app/interfaces/voiture';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';  
import swal from 'sweetalert2';



@Component({
  selector: 'app-liste-contrats',
  templateUrl: './liste-contrats.component.html',
  styleUrls: ['./liste-contrats.component.css']
})
export class ListeContratsComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private serviceFamille:FamilleProduitService,private service:ContratService,private serviceClient:ParticulierService,private serviceRisque:RisqueService,private serviceGarantie:GarantieService) { }
  contrats:any[]=[];
  client:any[]=[];
  risque:Risque[]=[];
  famille:FamilleProduit[]=[];
  voiture:Voiture[]=[];
  contratsGarantie:[]=[];
  garantie:any[]=[];
  InvalidFromAvenant;

  ngOnInit() {
    this.getAll();
    this.getAllClient();
    this.getAllRisque();
    this.getAllFamille();
    this.getAllVoiture();
    this.getContratsGarantie();
    this.getAllGarantie();
    this.loadScripts();
 }

 public getAll(){
   
  return this.service.search().subscribe(data=>{  
    this.contrats= data
  });
  
}

public getAllVoiture(){
   
  return this.service.searchVoiture().subscribe(data=>{  
    this.voiture= data
  });
  
}

public getAllClient(){
  return this.serviceClient.search().subscribe(data=>{
   
  this.client= data["client"].map(el=>{
     return {
       id:el.id,
       nom:el.nom,
       email:el.email,
       adresse:el.adresse,
       tel:el.tel,
       mobile:el.mobile
      
      }
      
   })



  })



}


public getAllRisque(){
  return this.serviceRisque.search().subscribe(data=>{
   
  this.risque= data["risque"].map(el=>{
     return {
       id:el.id,
       code:el.code,
       nom:el.nom
      
      }
   })



   

  })
}

public getAllFamille(){
   
  return this.serviceFamille.search().subscribe(data=>{  
    this.famille= data["familleproduit"].map(el=>{   
  
      return {
       id:el.id,
       code:el.code,
       nom:el.nom,
       risque:el.risqueId   
      }
   })


  })
}

public getContratsGarantie(){
   
  return this.service.searchContratGarantie().subscribe(data=>{  
  console.log(data);  
   this.contratsGarantie=data
  })

}

public getAllGarantie(){
   
  return this.serviceGarantie.search().subscribe(data=>{  
    this.garantie= data["garantie"].map(el=>{   
  
      return {
       id:el.id,
       nom:el.nom,
       niveau:el.niveau,
       montantAssure:el.montantAssure,
       montantFranchise:el.montantFranchise,
       familleproduit:el.familleproduitId   
      }
   })

  })
}
print(i){
    
    
  var data = document.getElementById('HTMLtoPDF'+i);  
  html2canvas(data).then(canvas => {  
    // Few necessary setting options  
    var imgWidth = 208;   
    var pageHeight = 295;    
    var imgHeight = canvas.height * imgWidth / canvas.width;  
    var heightLeft = imgHeight;  

    const contentDataURL = canvas.toDataURL('image/png')  
    let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
    var position = 0;  
    pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, 280)  
    pdf.save('contrats.pdf'); // Generated PDF   
  });
   
}

public createAvenant(f){
  return this.service.createAvenant(f).subscribe(data =>{ console.log(data);
    this.succes();
 },error=>{
   this.InvalidFromAvenant=error["error"]["message"]; 
 
 })
}

succes(): void{
  swal.fire(
    'Creation Avenant!',
    'Votre avez creer un nouveau avenant!',
    'success'
  )
  
  }

  

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }
}

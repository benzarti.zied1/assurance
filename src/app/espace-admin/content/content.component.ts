import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services/user.service';
import {User} from '../../interfaces/user';
import { Router } from '@angular/router';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  user : User = {
    id:'',
    username: '',
    email:'',
    password:''
  };
  IsFormNonValid=false;
  form: any = {};
  roles: string[] = [];
  errorMessage;
  isLoggedIn = false;
  isLoginFailed = false;
token:string=this.tokenStorage.getToken();
  constructor(private service:UserService,private router: Router,private tokenStorage: TokenStorageService)
   { }

  ngOnInit(): void {
  
    if (this.tokenStorage.getToken()) {
      //this.router.navigate(['/devis']);

    }
    else{
     // this.router.navigate(['/']);

    }
  
  console.log(this.token);
  }
  
  onSubmit() {
    this.service.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
  
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.lienmenu();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }
    lienmenu(){
      this.router.navigate(['accueil']);
    }
    
}

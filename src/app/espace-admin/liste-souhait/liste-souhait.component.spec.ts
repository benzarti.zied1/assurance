import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeSouhaitComponent } from './liste-souhait.component';

describe('ListeSouhaitComponent', () => {
  let component: ListeSouhaitComponent;
  let fixture: ComponentFixture<ListeSouhaitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeSouhaitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeSouhaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

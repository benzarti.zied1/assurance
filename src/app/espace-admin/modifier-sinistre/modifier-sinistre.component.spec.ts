import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierSinistreComponent } from './modifier-sinistre.component';

describe('ModifierSinistreComponent', () => {
  let component: ModifierSinistreComponent;
  let fixture: ComponentFixture<ModifierSinistreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierSinistreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierSinistreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

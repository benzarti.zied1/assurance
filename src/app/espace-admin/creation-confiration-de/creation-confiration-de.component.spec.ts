import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationConfirationDeComponent } from './creation-confiration-de.component';

describe('CreationConfirationDeComponent', () => {
  let component: CreationConfirationDeComponent;
  let fixture: ComponentFixture<CreationConfirationDeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationConfirationDeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationConfirationDeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Societe } from 'src/app/interfaces/societe';
import { SocieteService } from 'src/app/services/societe.service';
import { Particulier } from 'src/app/interfaces/particulier';
import swal from 'sweetalert2';

@Component({
  selector: 'app-creation-client',
  templateUrl: './creation-client.component.html',
  styleUrls: ['./creation-client.component.css']
})
export class CreationClientComponent implements OnInit {
  InvalidFromParticulier;
  InvalidFromSociete;
  particulier : any = {
    nomP:'',
    cin:'', 
    adresseP:'',
    emailP:'',
    telP:'',
    mobileP:''
  };

  societe : Societe = {
    nom:'',
    adresse:'',
    email:'',
    tel:'',
    mobile:'',
    raison_sociale:'',
    numero_registre:'', 
    date_creation:null,
    siteWeb:'',
    fax:'',
  };
  constructor(private service:ParticulierService,private serviceSociete:SocieteService) { }

  ngOnInit(): void {
  }

  saveParticulier(particulier:Particulier) {
    
  
    this.service.addParticulier(particulier)
     .subscribe(data =>{ console.log(data);
      this.succes();
  
        },
     
     error => {
      this.InvalidFromParticulier=error["error"]["message"]; 
      console.log(error);
    });
  }

  saveSociete(societe:Societe) {
    
  
    this.serviceSociete.addSociete(societe)
     .subscribe(data =>{ console.log(data);
      this.succes();
    
        },
     
     error => {
      this.InvalidFromSociete=error["error"]["message"]; 

    });
   // console.log(societe);
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau client!',
      'success'
    ).then(()=>{
      location.reload();
    })

  }

}

import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Societe } from 'src/app/interfaces/societe';
import { Particulier } from 'src/app/interfaces/particulier';
import swal from 'sweetalert2';
import { error } from 'protractor';
@Component({
  selector: 'app-liste-clients',
  templateUrl: './liste-clients.component.html',
  styleUrls: ['./liste-clients.component.css']
})
export class ListeClientsComponent implements OnInit {
  InvalidFromParticulier;
  InvalidFromSociete;
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:ParticulierService) { }
  a:any={
    id:'',
    nom:'',
    email:'',
    tel:'',
    mobile:'',
    adresse:'',
    cin:''
  }

  s:any={
    id:'',
    nom:'',
    email:'',
    tel:'',
    mobile:'',
    adresse:'',
    raison_sociale:'',
    fax:'',
    numero_registre:'',
    date_creation:'',
    siteWeb:''
  }
  client:any[]=[];
particulier;
societe;
c;
p;
su:Societe[]=[];
pu:Particulier[]=[];
particuliers:any[]=[];
societes:Societe[]=[];

  ngOnInit() {
    
     this.loadScripts();
     this.getAll();
     this.getAllParticulier();
     this.getAllSociete();

     // this.getOneSociete(this.client.id);
     //this.getOneParticulier(this.client.id);

  }
  
  public getAll(){
    return this.service.search().subscribe(data=>{
     
    this.client= data["client"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })
 
 

    })

  

  }

  public getAllParticulier(){
    
    return this.service.searchParticulier().subscribe(data=>{
     
      this.client= data["client"].map(el=>{
         return {
           id:el.id,
           nom:el.nom,
           email:el.email,
           adresse:el.adresse,
           tel:el.tel,
           mobile:el.mobile
          
          }
          
       })

       this.particulier= data["particulier"].map(el=>{
        return {
          id:el.id,
          cin:el.cin
         
         }
         
      })



      for(let i=0;i<this.client.length;i++){
        this.a={
          id:this.client[i].id,
          nomP:this.client[i].nom,
          emailP:this.client[i].email,
          telP:this.client[i].tel,
          mobileP:this.client[i].mobile,
          adresseP:this.client[i].adresse,
          cin:this.particulier[i].cin
        }
        this.particuliers.push(this.a);
        
            }
   
   
  
      })
  

  }


  public getAllSociete(){
    
    return this.service.searchSociete().subscribe(data=>{
     
      this.client= data["client"].map(el=>{
         return {
           id:el.id,
           nom:el.nom,
           email:el.email,
           adresse:el.adresse,
           tel:el.tel,
           mobile:el.mobile
          
          }
          
       })

       this.societe= data["societe"].map(el=>{
        return {
          id:el.id,
          raison_sociale:el.raison_sociale,
          fax:el.fax,
          numero_registre:el.numero_registre,
          date_creation:el.date_creation,
          siteWeb:el.siteWeb
         }
         
      })

      

      for(let i=0;i<this.client.length;i++){
        this.s={
          id:this.client[i].id,
          nom:this.client[i].nom,
          email:this.client[i].email,
          tel:this.client[i].tel,
          mobile:this.client[i].mobile,
          adresse:this.client[i].adresse,
          raison_sociale:this.societe[i].raison_sociale,
          fax:this.societe[i].fax,
          numero_registre:this.societe[i].numero_registre,
          date_creation:this.societe[i].date_creation,
          siteWeb:this.societe[i].siteWeb
        }
        this.societes.push(this.s);
        
            }
   
   
  
      })
  

  }

public check(id){
  return this.service.checkClient(id).subscribe(data=>{
    this.c=data;
  })
}

updateParticulier(particulier){
  this.service.updateParticulier(particulier)
  .subscribe(data =>{ console.log(data);
   this.succes();
},error=>{
  this.InvalidFromParticulier=error["error"]["message"]; 

})
}

updateSociete(societe){
  this.service.updateSociete(societe)
  .subscribe(data =>{ console.log(data);
   this.succes();
},error=>{
  this.InvalidFromSociete=error["error"]["message"]; 

})
}

deleteParticulier(id){
  return this.service.deleteParticulier(id).subscribe(data=>{
    location.reload();
  })
}

deleteSociete(id){
  return this.service.deleteSociete(id).subscribe(data=>{
    location.reload();
  })
}

succes(): void{
swal.fire(
  'Modification!',
  'Votre avez modifier votre Client!',
  'success'
)

}

deletePopupParticulier(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer le client "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer le client avec succes.',
        'success'
      ).then(()=>{
        this.deleteParticulier(id);
      })
    }
  })
}

deletePopupSociete(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer le client "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer le client avec succes.',
        'success'
      ).then(()=>{
        this.deleteSociete(id);
      })
    }
  })
}


  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }
}

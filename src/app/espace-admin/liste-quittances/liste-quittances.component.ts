import { Component, OnInit, Directive } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { QuittanceService } from 'src/app/services/quittance.service';
import { ContratService } from 'src/app/services/contrat.service';
import { ParticulierService } from 'src/app/services/particulier.service';


@Component({
  selector: 'app-liste-quittances',
  templateUrl: './liste-quittances.component.html',
  styleUrls: ['./liste-quittances.component.css']
})

export class ListeQuittancesComponent implements OnInit {
  quittances:any[]=[];
  contrats:any[]=[];
  client:any[]=[];
  factures:any[]=[];
  ligneFactures:any[]=[];
  somme:number=0;
  total:number=0;
  etat:Array<string>=[];

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:QuittanceService,private serviceContrat:ContratService,private serviceClient:ParticulierService) { }

  ngOnInit() {
    this.getAll();
    this.getAllContrat();
    this.getAllClient();
    this.getAllFacture();
    this.getAllLigneFactures();
     this.loadScripts();
  }

  public getAll(){
   
    return this.service.search().subscribe(data=>{  
      this.quittances= data
      for(let i in data){
        var date=new Date()
        
        var datefin=new Date(data[i].perdiodeAu);
       
        var nb = datefin.getTime() - date.getTime();
        var nbjour= nb / (1000 * 3600 * 24);
        if(nbjour>0)
        this.etat[i]="ouvert";
        else
        this.etat[i]="fermé";
      
      }
     
    });
  }

  public getAllContrat(){
   
    return this.serviceContrat.search().subscribe(data=>{  
      this.contrats= data
    });
  }

  public getAllClient(){
    return this.serviceClient.search().subscribe(data=>{
     
    this.client= data["client"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })
 
 

    })

  

  }

  public getAllFacture(){
    return this.service.searchFacture().subscribe(data=>{
    this.factures= data;
    })
  }

  public getAllLigneFactures(){
    return this.service.searchLigneFacture().subscribe(data=>{
    this.ligneFactures= data;
    })
  }
public print(i){

}
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeQuittancesComponent } from './liste-quittances.component';

describe('ListeQuittancesComponent', () => {
  let component: ListeQuittancesComponent;
  let fixture: ComponentFixture<ListeQuittancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeQuittancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeQuittancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

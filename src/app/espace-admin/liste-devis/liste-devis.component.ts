import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { DevisService } from 'src/app/services/devis.service';
import { Devis } from 'src/app/interfaces/devis';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Risque } from 'src/app/interfaces/risque';
import { RisqueService } from 'src/app/services/risque.service';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { Voiture } from 'src/app/interfaces/voiture';
import swal from 'sweetalert2';
import { GarantieService } from 'src/app/services/garantie.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';  




@Component({
  selector: 'app-liste-devis',
  templateUrl: './liste-devis.component.html',
  styleUrls: ['./liste-devis.component.css']
})
export class ListeDevisComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private serviceFamille:FamilleProduitService,private service:DevisService,private serviceClient:ParticulierService,private serviceRisque:RisqueService,private serviceGarantie:GarantieService) { }
  devis:Devis[]=[];
  client:any[]=[];
  risque:Risque[]=[];
  famille:FamilleProduit[]=[];
  voiture:Voiture[]=[];
  devisGarantie:[]=[];
  garantie:any[]=[];

  ngOnInit() {
    this.getAll();
    this.getAllClient();
    this.getAllRisque();
    this.getAllFamille();
    this.getAllVoiture();
    this.getDevisGarantie();
    this.getAllGarantie();
    this.loadScripts();
 }



 public getAll(){
   
  return this.service.search().subscribe(data=>{  
    this.devis= data
  });
  
}

public getAllVoiture(){
   
  return this.service.searchVoiture().subscribe(data=>{  
    this.voiture= data
  });
  
}

public getAllClient(){
  return this.serviceClient.search().subscribe(data=>{
   
  this.client= data["client"].map(el=>{
     return {
       id:el.id,
       nom:el.nom,
       email:el.email,
       adresse:el.adresse,
       tel:el.tel,
       mobile:el.mobile
      
      }
      
   })



  })



}


public getAllRisque(){
  return this.serviceRisque.search().subscribe(data=>{
   
  this.risque= data["risque"].map(el=>{
     return {
       id:el.id,
       code:el.code,
       nom:el.nom
      
      }
   })



   

  })
}

public getAllFamille(){
   
  return this.serviceFamille.search().subscribe(data=>{  
    this.famille= data["familleproduit"].map(el=>{   
  
      return {
       id:el.id,
       code:el.code,
       nom:el.nom,
       risque:el.risqueId   
      }
   })


  })
}

public getDevisGarantie(){
   
  return this.service.searchDevisGarantie().subscribe(data=>{  
    
   this.devisGarantie=data
  })
}

public getAllGarantie(){
   
  return this.serviceGarantie.search().subscribe(data=>{  
    this.garantie= data["garantie"].map(el=>{   
  
      return {
       id:el.id,
       code:el.code,
       nom:el.nom,
       niveau:el.niveau,
       montantAssure:el.montantAssure,
       montantFranchise:el.montantFranchise,
       familleproduit:el.familleproduitId   
      }
   })

console.log(this.garantie);
  })
}

delete(id){
  return this.service.delete(id).subscribe(data=>{
    location.reload();
  })
}

deletePopup(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer le devis "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer le Famille Produit avec succes.',
        'success'
      ).then(()=>{
        this.delete(id);
      })
    }
  })
}

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }

  print(i){
    
    
    var data = document.getElementById('HTMLtoPDF'+i);  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, 280)  
      pdf.save('devis.pdf'); // Generated PDF   
    });
     
  }

}

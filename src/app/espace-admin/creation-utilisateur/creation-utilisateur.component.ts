import { Component, OnInit } from '@angular/core';
import { UtilisateurService} from '../../services/utilisateur.service';
import {Utilisateur} from '../../interfaces/utilisateur';
import swal from 'sweetalert2';
@Component({
  selector: 'app-creation-utilisateur',
  templateUrl: './creation-utilisateur.component.html',
  styleUrls: ['./creation-utilisateur.component.css']
})
export class CreationUtilisateurComponent implements OnInit {
  InvalidFrom;
  utilisateur = {
    email: '',
    username: '',
    password:'',
    nom:'',
    prenom:'',
    cin:'',
    tel:'',
    autreTel:'',
    ville:'',
    date_naissance:null,
    codePostal:'',
    role:''
  };
  IsFormNonValid=false;

  constructor(private service:UtilisateurService) { }

  ngOnInit(): void {
  }
  save(utilisateur:Utilisateur) {
    
    console.log(utilisateur)
    this.service.add(this.utilisateur)
     .subscribe(data =>{ console.log(data);
      this.succes();
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 
      console.log(error);
    });
    
  }

  succes(): void{
    swal.fire(
      'Incription!',
      'Votre avez créer un nouveau utilisateur!',
      'success'
    )

  }


}

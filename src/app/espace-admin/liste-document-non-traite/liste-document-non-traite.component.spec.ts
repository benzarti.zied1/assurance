import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDocumentNonTraiteComponent } from './liste-document-non-traite.component';

describe('ListeDocumentNonTraiteComponent', () => {
  let component: ListeDocumentNonTraiteComponent;
  let fixture: ComponentFixture<ListeDocumentNonTraiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDocumentNonTraiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDocumentNonTraiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

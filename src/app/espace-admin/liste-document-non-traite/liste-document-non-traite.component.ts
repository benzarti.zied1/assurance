import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FileService } from 'src/app/services/file.service';
import swal from 'sweetalert2';
import { FileMimeType } from '@taldor-ltd/angular-file-viewer';
import { ParticulierService } from 'src/app/services/particulier.service';




@Component({
  selector: 'app-liste-document-non-traite',
  templateUrl: './liste-document-non-traite.component.html',
  styleUrls: ['./liste-document-non-traite.component.css']
})
export class ListeDocumentNonTraiteComponent implements OnInit {
  type = FileMimeType.PDF;
  //src= "assets/app-assets/images/Dropbox/Assurance/2020-04-07 01.28.28.pdf";

  

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:FileService,private serviceClient:ParticulierService) { }
files=[];
client:any[]=[];
selectedValueClient;
InvalidFrom;
image=null;

  ngOnInit() {
    
    this.loadScripts();
    this.getAll();
    
    this.getAllClient();
 }

 save(f){
  this.service.add(f)
  .subscribe(data =>{
   this.succes();
     },
  
  error => {//console.log(error);
   this.InvalidFrom=error["error"]["message"]; 
   console.log(error);
 });
 }
 succes(): void{
  swal.fire(
    'Traitement Document!',
    'Votre avez créer un nouveau fichier avec succees!',
    'success'
  )

}






 public getAll(){
   
  return this.service.search().subscribe(data=>{  
    console.log(data);
    this.files=data;
   })
}

public getAllClient(){
  return this.serviceClient.search().subscribe(data=>{
   
  this.client= data["client"].map(el=>{
     return {
       id:el.id,
       nom:el.nom,
       email:el.email,
       adresse:el.adresse,
       tel:el.tel,
       mobile:el.mobile
      
      }
      
   })



  })



}

delete(document){
  return this.service.delete(document).subscribe(data=>{
    location.reload();
  })
}

deletePopup(document){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer le document "+document,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer le document avec succes.',
        'success'
      ).then(()=>{
        this.delete(document);
      })
    }
  })
}

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2','images').then(data => {
   }).catch(error => console.log(error));
 }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeRisqueComponent } from './liste-risque.component';

describe('ListeRisqueComponent', () => {
  let component: ListeRisqueComponent;
  let fixture: ComponentFixture<ListeRisqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeRisqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeRisqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

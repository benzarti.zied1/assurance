import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { RisqueService } from 'src/app/services/risque.service';
import { Risque } from 'src/app/interfaces/risque';
import swal from 'sweetalert2';
import { HttpClient, HttpHeaders,HttpClientModule,HttpEventType } from '@angular/common/http';



@Component({
  selector: 'app-liste-risque',
  templateUrl: './liste-risque.component.html',
  styleUrls: ['./liste-risque.component.css']
})
export class ListeRisqueComponent implements OnInit {
  InvalidFrom;
  selectedFile : File = null;

  constructor(private http: HttpClient,private dynamicScriptLoader: DynamicFileAdminServiceService,private service:RisqueService) { }

  ngOnInit() {
    
     this.loadScripts();
     this.getAll();

  }
  risque:Risque[]=[];

  public getAll(){
    return this.service.search().subscribe(data=>{
     
    this.risque= data["risque"].map(el=>{
       return {
         id:el.id,
         image:el.image,
         nom:el.nom
        
        }
     })

 
  
     

    })
  }
  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }

  deletePopup(id,nom){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer le Risque "+nom,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer le Risque avec succes.',
          'success'
        ).then(()=>{
          this.delete(id);
        })
      }
    })
  }
  
  update(risque){
    this.onUpload();
    risque.image=this.selectedFile.name;
    this.service.update(risque)
    .subscribe(data =>{ 
     this.succes();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }
  succes(): void{
    swal.fire(
      'Modification!',
      'Votre avez modifier votre Risque!',
      'success'
    ).then(()=>{
      location.reload();
    })
    
    }  
    onFileSelected(event){
      this.selectedFile = <File> event.target.files[0];
    }
    onUpload(){
      const fd = new FormData();
      fd.append('image', this.selectedFile, this.selectedFile.name);
     // console.log(fd);
      //console.log(this.selectedFile.name);
      this.http.post('http://localhost:3000/single', fd)
        .subscribe(res => {
          console.log(res);
        });
      
      //this.updateAgence(this.agence);
    
    }
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }

}

import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  roles=this.tokenStorage.getUser().roles[0];

  constructor(private tokenStorage: TokenStorageService,public router: Router,private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit(): void {
   // this.loadScripts();
console.log(this.roles);
   
  }

  private loadScripts() {
    this.dynamicScriptLoader.load('s3','s4','s5','s6','s7','s8').then(data => {
    }).catch(error => console.log(error));
  }

}

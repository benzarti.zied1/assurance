import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
//roles=this.tokenStorage.getUser().roles;
  constructor(private tokenStorage: TokenStorageService,private router: Router) { }
  roles=this.tokenStorage.getUser().roles[0];
  roleUser:String;
  id=this.tokenStorage.getUser().id;
  ngOnInit(): void {
    this.getRoleUtilisateur(this.id);
  }

  getRoleUtilisateur(id){
    return this.tokenStorage.getRoleUser(id).subscribe((data)=>{
      this.roleUser=data["role"];
      console.log(this.roleUser);

      //console.log(data["role"])
    })
  }

}

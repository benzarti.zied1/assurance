import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationRendezVousComponent } from './creation-rendez-vous.component';

describe('CreationRendezVousComponent', () => {
  let component: CreationRendezVousComponent;
  let fixture: ComponentFixture<CreationRendezVousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationRendezVousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationRendezVousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

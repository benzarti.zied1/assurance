import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import swal from 'sweetalert2';
import { RendezvousService } from 'src/app/services/rendezvous.service';
import { ParticulierService } from 'src/app/services/particulier.service';



@Component({
  selector: 'app-creation-rendez-vous',
  templateUrl: './creation-rendez-vous.component.html',
  styleUrls: ['./creation-rendez-vous.component.css']
})
export class CreationRendezVousComponent implements OnInit {
  InvalidFrom;
  clt:any[]=[];
  datetime;
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:RendezvousService,private serviceClient:ParticulierService) { }

  ngOnInit() {
    this.getAll();
     this.loadScripts();
  }
  save(rendezvous) {
    
  
    this.service.add(rendezvous)
     .subscribe(data =>{ console.log(data);
      this.succes();
  
        },
     
     error => {
      this.InvalidFrom=error["error"]["message"]; 
      console.log(error);
    });
  }
  public getAll(){
    return this.serviceClient.search().subscribe(data=>{
     
    this.clt= data["client"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })
 
 

    })

  

  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau client!',
      'success'
    ).then(()=>{
      location.reload();
    })

  }



 
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('s3').then(data => {
    }).catch(error => console.log(error));
  }

}

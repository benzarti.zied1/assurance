import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { Router, NavigationEnd }  from "@angular/router";
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Societe } from 'src/app/interfaces/societe';
import { SocieteService } from 'src/app/services/societe.service';
import { Particulier } from 'src/app/interfaces/particulier';
import swal from 'sweetalert2';
import { Risque } from 'src/app/interfaces/risque';
import { RisqueService } from 'src/app/services/risque.service';
import { Devis } from 'src/app/interfaces/devis';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { ThrowStmt } from '@angular/compiler';
import { GarantieService } from 'src/app/services/garantie.service';
import { Garantie } from 'src/app/interfaces/garantie';
import { User } from 'src/app/interfaces/user';
import { CompagnieService } from 'src/app/services/compagnie.service';
import { Compagnie } from 'src/app/interfaces/compagnie';
import { ApporteurService } from 'src/app/services/apporteur.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { DevisService } from 'src/app/services/devis.service';
@Component({
  selector: 'app-creation-devis',
  templateUrl: './creation-devis.component.html',
  styleUrls: ['./creation-devis.component.css']
})
export class CreationDevisComponent implements OnInit {
  particulier : any = {
    nomP:'',
    cin:'', 
    adresseP:'',
    emailP:'',
    telP:'',
    mobileP:''
  };

  societe : Societe = {
    nom:'',
    adresse:'',
    email:'',
    tel:'',
    mobile:'',
    raison_sociale:'',
    numero_registre:'', 
    date_creation:null,
    siteWeb:'',
    fax:'',
  };
  counter = Array;
  constructor(private service:DevisService,private serviceApporteur:ApporteurService,private serviceCompagnie:CompagnieService,private serviceGarantie:GarantieService,private serviceClient:ParticulierService,private serviceSociete:SocieteService,private serviceRisque:RisqueService,private serviceFamille:FamilleProduitService,private dynamicScriptLoader: DynamicFileAdminServiceService) {

   }

  ngOnInit() {
    
     this.loadScripts();
     this.getAllRisque();
     this.getAllFamille();
     this.getAllGarantie();
     this.getAllClient();
     this.getAllCompagnie();
     this.getAllApporteur();

    }
    client:any[]=[];
    risque:Risque[]=[];
    garantie:Garantie[]=[];
    gar:any[]=[];
    niveau:Garantie[]=[];
    devis:Devis;
    famille:FamilleProduit[]=[];
    selectedValue:string;
    selectedValueFamille:string;
    selectedValueClient:string;
    selectedValueGarantie:string;
    selectedValueApporteur:string;
    selectedValueCompagnie:string;
    selectedValueSinistre:number;
    selectedValueR1:string;
    

    InvalidFromSociete;
    InvalidFromParticulier;
    
  compagnies:any[]=[];
  apporteur:any []=[];

  console(f){
    console.log(f);
    
  }
  

   

save(f){
  f.famille=this.selectedValueFamille;
 this.service.add(f).subscribe(data =>{
  this.succesDevis();
     },
  
  error => {//console.log(error);
   console.log(error["error"]["message"]); 

 });
 console.log(f.risque);
     }
        public getAllCompagnie(){
          return this.serviceCompagnie.search().subscribe(data=>{
           
          this.compagnies= data["user"].map(el=>{
             return {
               id:el.id,
               username:el.username,
               email:el.email
               
              
              }
           })
   
          })
        }

        public getAllApporteur(){
          return this.serviceApporteur.search().subscribe(data=>{
           
          this.apporteur= data["apporteur"].map(el=>{
             return {
               id:el.id,
               nom:el.nom,
               email:el.email,
               adresse:el.adresse,
               tel:el.tel,
               mobile:el.mobile
              
              }
              
           })
       
       
      
          })
      
        
      
        }


    public getAllRisque(){
      return this.serviceRisque.search().subscribe(data=>{
       
      this.risque= data["risque"].map(el=>{
         return {
           id:el.id,
           image:el.image,
           nom:el.nom
          
          }
       })    
  
      })
    }

    public getAllFamille(){
   
      return this.serviceFamille.search().subscribe(data=>{  
        this.famille= data["familleproduit"].map(el=>{   
      
        console.log("el.risquye == "+el.risqueId);
          return {
           id:el.id,
           image:el.image,
           nom:el.nom,
           risque:el.risqueId   
          }
       })
    
  
      })
    }

    public getAllGarantie(){
   
      return this.serviceGarantie.search().subscribe(data=>{  
        this.garantie= data["garantie"].map(el=>{   
      
          return {
           id:el.id,
           nom:el.nom,
           niveau:el.niveau,
           montantAssure:el.montantAssure,
           montantFranchise:el.montantFranchise,
           familleproduit:el.familleproduitId   
          }
       })
       var ligne:Garantie[]=[];
       ligne.push(this.garantie[0]);
       for(let i=1;i<this.garantie.length;i++){
        
        var fam=this.garantie[i].familleproduit;
         var niv=this.garantie[i].niveau;
         var ok=false;
       
       for(let j=0;j<ligne.length;j++){
        if(niv==ligne[j].niveau && fam==ligne[j].familleproduit){
          ok=true;
        }
       }
      if(!ok){
      ligne.push(this.garantie[i]);
      
      }
         
        
       
       }
       this.niveau=ligne;
      })
    }

    public getAllClient(){
      return this.serviceClient.search().subscribe(data=>{
       
      this.client= data["client"].map(el=>{
         return {
           id:el.id,
           nom:el.nom,
           email:el.email,
           adresse:el.adresse,
           tel:el.tel,
           mobile:el.mobile
          
          }
          
       })
   
   
  
      })
  
    
  
    }


    deleteRow (index:number) {
      this.garantie.splice(index, 1); //replace your Model here instead of this.user
   }
    saveParticulier(particulier:Particulier) {
    
  
      this.serviceClient.addParticulier(particulier)
       .subscribe(data =>{ console.log(data);
        this.succes();
        this.getAllClient();
    
          },
       
       error => {
        this.InvalidFromParticulier=error["error"]["message"]; 
        console.log(error);
      });
    }
  
    saveSociete(societe:Societe) {
      
    
      this.serviceSociete.addSociete(societe)
       .subscribe(data =>{ console.log(data);
        this.succes();
      this.getAllClient();
          },
       
       error => {
        this.InvalidFromSociete=error["error"]["message"]; 
  
      });
     // console.log(societe);
    }

  succes(): void{
    swal.fire(
      'Incription!',
      'Votre avez créer un nouveau client!',
      'success'
    )

  }

  succesDevis(): void{
    swal.fire(
      'Incription!',
      'Votre avez créer un nouveau devis!',
      'success'
    ).then(()=>{
      location.reload();
    })

  }
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('s1').then(data => {
    }).catch(error => console.log(error));
  }
  

}

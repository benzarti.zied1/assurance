import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { Risque } from 'src/app/interfaces/risque';
import { RisqueService } from 'src/app/services/risque.service';
import { HttpClient, HttpHeaders,HttpClientModule,HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-creation-risque',
  templateUrl: './creation-risque.component.html',
  styleUrls: ['./creation-risque.component.css']
})
export class CreationRisqueComponent implements OnInit {
  InvalidFrom;
  selectedFile : File = null;

  risque : Risque = {
    image: '',
    nom: '',
  };
  constructor(private http: HttpClient,private service:RisqueService) { }

  ngOnInit(): void {
  }

  save(risque:Risque) {
    this.onUpload();
    risque.image=this.selectedFile.name;
    this.service.add(risque)
     .subscribe(data =>{ console.log(data);
      this.succes();
    
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 

    });
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau Risque!',
      'success'
    )

  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
  }
  
  onUpload(){
    const fd = new FormData();
    fd.append('image', this.selectedFile, this.selectedFile.name);
   // console.log(fd);
    //console.log(this.selectedFile.name);
    this.http.post('http://localhost:3000/single', fd)
      .subscribe(res => {
        console.log(res);
      });
    
    console.log(this.risque.image);
    //this.updateAgence(this.agence);
  
  }

}

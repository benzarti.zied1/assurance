import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationRisqueComponent } from './creation-risque.component';

describe('CreationRisqueComponent', () => {
  let component: CreationRisqueComponent;
  let fixture: ComponentFixture<CreationRisqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationRisqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationRisqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

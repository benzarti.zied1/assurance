import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';


@Component({
  selector: 'app-modifier-devis',
  templateUrl: './modifier-devis.component.html',
  styleUrls: ['./modifier-devis.component.css']
})
export class ModifierDevisComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit() {
    
     this.loadScripts();
  }

  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('s1').then(data => {
    }).catch(error => console.log(error));
  }

}

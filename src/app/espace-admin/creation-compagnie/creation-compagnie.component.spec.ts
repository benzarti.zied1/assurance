import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationCompagnieComponent } from './creation-compagnie.component';

describe('CreationCompagnieComponent', () => {
  let component: CreationCompagnieComponent;
  let fixture: ComponentFixture<CreationCompagnieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationCompagnieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationCompagnieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Compagnie } from '../../interfaces/compagnie';
import { CompagnieService } from '../../services/compagnie.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-creation-compagnie',
  templateUrl: './creation-compagnie.component.html',
  styleUrls: ['./creation-compagnie.component.css']
})
export class CreationCompagnieComponent implements OnInit {
  InvalidFrom;
  compagnie : Compagnie = {
    nom:'',
    email: '',
    username: '',
    password:'',
    siteWeb:'',
    tel:'',
    specialite:'',
    status:''
  };
  constructor(private service:CompagnieService) { }

  ngOnInit(): void {
  }
  save(compagnie:Compagnie) {
    
    console.log(compagnie)
    this.service.add(compagnie)
     .subscribe(data =>{ console.log(data);
      this.succes();
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 
      console.log(error);
    });
  }

  succes(): void{
    swal.fire(
      'Incription!',
      'Votre avez créer un nouveau Compagnie!',
      'success'
    )

  }
}

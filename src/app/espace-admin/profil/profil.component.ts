import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Utilisateur } from 'src/app/interfaces/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  InvalidFrom;
  utilisateur;
  agence;
  admin;

  constructor(private tokenStorage: TokenStorageService,private service:UtilisateurService) { }
  roles=this.tokenStorage.getUser().roles[0];

  ngOnInit(): void {

console.log(this.roles);
console.log(this.tokenStorage.getUser().id);
this.getOneUtilisateur();
this.getOneAgence();
this.getOneAdmin();
  }

  updateProfilUtilisateur(f){
    this.service.updateOne(f,this.tokenStorage.getUser().id)
    .subscribe(data =>{ 
    if(data["message"]=="success")
      this.succes();

     })

  }

  updateProfilAgence(f){
    this.service.updateOneAgence(f,this.tokenStorage.getUser().id)
    .subscribe(data =>{ 
    if(data["message"]=="success")
      this.succes();

     })

  }

  getOneUtilisateur(){

    this.service.getOneUtilisateur(this.tokenStorage.getUser().id)
    .subscribe(data =>{ 
      this.utilisateur=data;
      console.log(data);

     })

}

getOneAdmin(){

  this.service.getOneAdmin(this.tokenStorage.getUser().id)
  .subscribe(data =>{ 
    this.admin=data;
   
   })

}

getOneAgence(){

  this.service.getOneAgence(this.tokenStorage.getUser().id)
  .subscribe(data =>{ 
    this.agence=data;
    console.log(data);

   })

}
changePasswordUser(f){
  this.service.changePasswordUtilisateur(f,this.tokenStorage.getUser().email)
  .subscribe(data =>{ 
    if(data["message"]=="success")
    this.succes();

console.log(data);
   },   error => {//console.log(error);
    this.InvalidFrom=error["error"]["message"]; 
    console.log(error)})
  }

succes(): void{
  swal.fire(
    'Modification!',
    'Votre modification été effectué avec success!',
    'success'
  )

}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeReglementsComponent } from './liste-reglements.component';

describe('ListeReglementsComponent', () => {
  let component: ListeReglementsComponent;
  let fixture: ComponentFixture<ListeReglementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeReglementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeReglementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

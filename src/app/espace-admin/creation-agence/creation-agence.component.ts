import { Component, OnInit } from '@angular/core';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interfaces/agence';
import swal from 'sweetalert2';

@Component({
  selector: 'app-creation-agence',
  templateUrl: './creation-agence.component.html',
  styleUrls: ['./creation-agence.component.css']
})
export class CreationAgenceComponent implements OnInit {
  InvalidFrom;
 agence : Agence = {
    email: '',
    username: '',
    password:'',
    siteWeb:'',
    nom:'',
    tel:''
  };
  IsFormNonValid=false;

  constructor(private service:AgenceService) { }

  ngOnInit(): void {
  }
  save(agence:Agence) {
    
    console.log(agence)
    this.service.add(agence)
     .subscribe(data =>{ console.log(data);
      this.succes();
    
        },
     
     error => {//console.log(error);
      this.InvalidFrom=error["error"]["message"]; 

    });
  }

  succes(): void{
    swal.fire(
      'Inscription!',
      'Votre avez créer un nouveau agence!',
      'success'
    )

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationAgenceComponent } from './creation-agence.component';

describe('CreationAgenceComponent', () => {
  let component: CreationAgenceComponent;
  let fixture: ComponentFixture<CreationAgenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationAgenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationAgenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

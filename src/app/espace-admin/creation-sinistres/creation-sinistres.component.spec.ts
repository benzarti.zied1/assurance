import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationSinistresComponent } from './creation-sinistres.component';

describe('CreationSinistresComponent', () => {
  let component: CreationSinistresComponent;
  let fixture: ComponentFixture<CreationSinistresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationSinistresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationSinistresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

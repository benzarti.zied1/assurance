import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeSinistresComponent } from './liste-sinistres.component';

describe('ListeSinistresComponent', () => {
  let component: ListeSinistresComponent;
  let fixture: ComponentFixture<ListeSinistresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeSinistresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeSinistresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';


@Component({
  selector: 'app-liste-sinistres',
  templateUrl: './liste-sinistres.component.html',
  styleUrls: ['./liste-sinistres.component.css']
})
export class ListeSinistresComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit() {
    
    this.loadScripts();
 }

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }

}

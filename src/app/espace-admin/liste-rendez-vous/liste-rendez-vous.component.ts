import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { RendezvousService } from 'src/app/services/rendezvous.service';
import { Rendezvous } from 'src/app/interfaces/rendezvous';
import { ParticulierService } from 'src/app/services/particulier.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-liste-rendez-vous',
  templateUrl: './liste-rendez-vous.component.html',
  styleUrls: ['./liste-rendez-vous.component.css']
})
export class ListeRendezVousComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:RendezvousService,private serviceClient:ParticulierService) { }

rendezvous;
InvalidFrom;
clt:any[]=[];

  ngOnInit() {
    this.getAll();
    this.getAllClient();
    this.loadScripts();
 }
 public getAll(){
  return this.service.search().subscribe(data=>{ 
  this.rendezvous= data;
  /*var d=new Date(this.rendezvous[0].date);
  this.rendezvous.date=d;
  console.log(this.rendezvous);
  console.log(d);
  */  
   })
}

public getAllClient(){
  
    return this.serviceClient.search().subscribe(data=>{
     
    this.clt= data["client"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })

    })

}

public update(f){
  this.service.update(f)
  .subscribe(data =>{ 
   this.succes();
},error=>{
  this.InvalidFrom=error["error"]["message"]; 

})
}
succes(): void{
  swal.fire(
    'Modification!',
    'Votre avez modifier votre Rendez vous!',
    'success'
  )
  
  } 


  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }

  deletePopup(id,description){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer le garantie "+description,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer le rendez-vous avec succes.',
          'success'
        ).then(()=>{
          this.delete(id);
        })
      }
    })
  }

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }

}

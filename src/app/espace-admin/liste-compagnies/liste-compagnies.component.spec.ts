import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeCompagniesComponent } from './liste-compagnies.component';

describe('ListeCompagniesComponent', () => {
  let component: ListeCompagniesComponent;
  let fixture: ComponentFixture<ListeCompagniesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeCompagniesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeCompagniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

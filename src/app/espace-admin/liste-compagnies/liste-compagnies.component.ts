import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { User } from 'src/app/interfaces/user';
import { Compagnie } from 'src/app/interfaces/compagnie';
import { CompagnieService } from 'src/app/services/compagnie.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-liste-compagnies',
  templateUrl: './liste-compagnies.component.html',
  styleUrls: ['./liste-compagnies.component.css']
})
export class ListeCompagniesComponent implements OnInit {
  InvalidFrom;
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:CompagnieService) { }
  user:User[]=[];
  compagnie:Compagnie[]=[];
  compagnies:Compagnie[]=[];
  
  a:any={      
    id:'',
    username:'',
    email:'',
    password:'',
    nom:'',
    tel:'',
    siteWeb:'',
    specialite:'',
    statuts:'',
   }
  ngOnInit() {
    
     this.loadScripts();
     this.getAll();

  }

  public getAll(){
    return this.service.search().subscribe(data=>{
     
    this.user= data["user"].map(el=>{
       return {
         id:el.id,
         username:el.username,
         email:el.email,
         password:el.password
        
        }
     })

     this.compagnie= data["compagnie"].map(el=>{
      return {
        id:el.id,
        nom:el.nom,
        tel:el.tel,
        siteWeb:el.siteWeb,
        specialite:el.specialite,
        status:el.status
       
       }
    })
    for(let i=0;i<this.compagnie.length;i++){
this.a={
  id:this.user[i].id,
  username:this.user[i].username,
  email:this.user[i].email,
  password:this.user[i].password,
  nom:this.compagnie[i].nom,
  tel:this.compagnie[i].tel,
  siteWeb:this.compagnie[i].siteWeb,
  specialite:this.compagnie[i].specialite,
  status:this.compagnie[i].status
}
this.compagnies.push(this.a);

    }
     

    })
  }
  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }


update(compagnie){
  this.service.update(compagnie)
  .subscribe(data =>{ console.log(data);
   this.succes();
},error=>{
  this.InvalidFrom=error["error"]["message"]; 

})
}
succes(): void{
  swal.fire(
    'Modification!',
    'Votre avez modifier votre Compagnie!',
    'success'
  )

}
deletePopup(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer compagnie "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer compagnie avec succes.',
        'success'
      ).then(()=>{
        this.delete(id);
      })
    }
  })
}




  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }

}

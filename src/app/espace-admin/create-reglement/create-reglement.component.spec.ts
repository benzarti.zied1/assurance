import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReglementComponent } from './create-reglement.component';

describe('CreateReglementComponent', () => {
  let component: CreateReglementComponent;
  let fixture: ComponentFixture<CreateReglementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateReglementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReglementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

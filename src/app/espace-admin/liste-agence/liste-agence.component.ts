import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { AgenceService } from 'src/app/services/agence.service';
import { User } from 'src/app/interfaces/user';
import { Agence } from 'src/app/interfaces/agence';
import swal from 'sweetalert2';



@Component({
  selector: 'app-liste-agence',
  templateUrl: './liste-agence.component.html',
  styleUrls: ['./liste-agence.component.css']
})
export class ListeAgenceComponent implements OnInit {
  InvalidFrom;

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:AgenceService) { }
  user:User[]=[];
  agence:Agence[]=[];
  agences:Agence[]=[];
  
  a:any={      
    id:'',
    username:'',
    email:'',
    password:'',
    nom:'',
    tel:'',
    siteWeb:''
   }
  
  ngOnInit() {
    
     this.loadScripts();
     this.getAll();
  }

  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }
  public getAll(){
    return this.service.search().subscribe(data=>{
     
    this.user= data["user"].map(el=>{
       return {
         id:el.id,
         username:el.username,
         email:el.email,
         password:el.password
        
        }
     })

     this.agence= data["agence"].map(el=>{
      return {
        id:el.id,
        nom:el.nom,
        tel:el.tel,
        siteWeb:el.siteWeb
       
       }
    })
    for(let i=0;i<this.agence.length;i++){
this.a={
  id:this.user[i].id,
  username:this.user[i].username,
  email:this.user[i].email,
  password:this.user[i].password,
  nom:this.agence[i].nom,
  tel:this.agence[i].tel,
  siteWeb:this.agence[i].siteWeb
}
this.agences.push(this.a);

    }
     

    })
  }

  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }
  update(agence){
    this.service.update(agence)
    .subscribe(data =>{ console.log(data);
     this.succes();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }
succes(): void{
  swal.fire(
    'Modification!',
    'Votre avez modifier votre Agence!',
    'success'
  )

}
deletePopup(id,nom){
  swal.fire({
    title: 'Vous étes sur?',
    text: "Vous avez supprimer apporteur "+nom,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui!'
  }).then((result) => {
    if (result.value) {
      swal.fire(
        'Suppression!',
        'Vous avez supprimer agence avec succes.',
        'success'
      ).then(()=>{
        this.delete(id);
      })
    }
  })
}


}

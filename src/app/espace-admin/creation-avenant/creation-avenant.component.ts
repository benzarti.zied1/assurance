import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { ContratService } from 'src/app/services/contrat.service';


@Component({
  selector: 'app-creation-avenant',
  templateUrl: './creation-avenant.component.html',
  styleUrls: ['./creation-avenant.component.css']
})
export class CreationAvenantComponent implements OnInit {
  InvalidFromAvenant;
  contrats:any[]=[];
  selectedValueContrat;
  constructor(private service:ContratService) { }

  ngOnInit(): void {
    this.getAll();
  }
  public getAll(){
   
    return this.service.search().subscribe(data=>{  
      this.contrats= data
    });
    
  }

  public createAvenant(f){
    return this.service.createAvenant(f).subscribe(data =>{ console.log(data);
      this.succes();
   },error=>{
     this.InvalidFromAvenant=error["error"]["message"]; 
   
   })
  }
  
  succes(): void{
    swal.fire(
      'Creation Avenant!',
      'Votre avez creer un nouveau avenant!',
      'success'
    )
    
    }
}

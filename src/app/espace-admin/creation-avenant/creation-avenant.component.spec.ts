import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationAvenantComponent } from './creation-avenant.component';

describe('CreationAvenantComponent', () => {
  let component: CreationAvenantComponent;
  let fixture: ComponentFixture<CreationAvenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationAvenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationAvenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

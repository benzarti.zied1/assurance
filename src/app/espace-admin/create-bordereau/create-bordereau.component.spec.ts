import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBordereauComponent } from './create-bordereau.component';

describe('CreateBordereauComponent', () => {
  let component: CreateBordereauComponent;
  let fixture: ComponentFixture<CreateBordereauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBordereauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBordereauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

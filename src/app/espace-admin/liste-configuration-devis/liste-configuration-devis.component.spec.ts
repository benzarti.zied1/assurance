import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeConfigurationDevisComponent } from './liste-configuration-devis.component';

describe('ListeConfigurationDevisComponent', () => {
  let component: ListeConfigurationDevisComponent;
  let fixture: ComponentFixture<ListeConfigurationDevisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeConfigurationDevisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeConfigurationDevisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

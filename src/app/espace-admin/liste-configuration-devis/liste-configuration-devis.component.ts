import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import swal from 'sweetalert2';
import { ConfigurationDevis } from 'src/app/interfaces/configuration-devis';
import { ConfigurationDevisService } from 'src/app/services/configuration-devis.service';
import { RisqueService } from 'src/app/services/risque.service';

@Component({
  selector: 'app-liste-configuration-devis',
  templateUrl: './liste-configuration-devis.component.html',
  styleUrls: ['./liste-configuration-devis.component.css']
})
export class ListeConfigurationDevisComponent implements OnInit {

  risque:any[]=[];
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService, private service:ConfigurationDevisService,private risqueService:RisqueService) { }
configuration:ConfigurationDevis[]=[];
InvalidFrom;

  ngOnInit() {
    
     this.loadScripts();
     this.getAll();
    this.getAllRisque();
  }

  public getAllRisque(){
    return this.risqueService.search().subscribe(data=>{
      this.risque= data["risque"];   
  
      })
    
  }
  public getAll(){
   
    return this.service.search().subscribe(data=>{  
      this.configuration= data["configurationdevis"].map(el=>{   
    
        return {
         id:el.id,
         code:el.code,
         questionnaire:el.questionnaire,
         risque:el.risqueId   
        }
     })
  

    })
  }

  delete(id){
    return this.service.delete(id).subscribe(data=>{
      location.reload();
    })
  }

  deletePopup(id,nom){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer le Configuration de devis "+nom,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer le Configuration de devis avec succes.',
          'success'
        ).then(()=>{
          this.delete(id);
        })
      }
    })
  }

  update(f){
    this.service.update(f)
    .subscribe(data =>{ 
     this.succes();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }
  succes(): void{
    swal.fire(
      'Modification!',
      'Votre avez modifier votre le Configuration de devis!',
      'success'
    )
    
    }  
 


  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }


}

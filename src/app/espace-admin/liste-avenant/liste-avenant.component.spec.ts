import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeAvenantComponent } from './liste-avenant.component';

describe('ListeAvenantComponent', () => {
  let component: ListeAvenantComponent;
  let fixture: ComponentFixture<ListeAvenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeAvenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeAvenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

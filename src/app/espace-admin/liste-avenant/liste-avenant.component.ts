import { Component, OnInit } from '@angular/core';
import { ContratService } from 'src/app/services/contrat.service';
import swal from 'sweetalert2';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';


@Component({
  selector: 'app-liste-avenant',
  templateUrl: './liste-avenant.component.html',
  styleUrls: ['./liste-avenant.component.css']
})
export class ListeAvenantComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:ContratService) { }
  InvalidFromAvenant;
  contrats:any[]=[];
  avenants:any[]=[];

  ngOnInit(): void {
    this.loadScripts();

    this.getAll();
    this.getAllContrat();
  }

  public getAll(){
   
    return this.service.searchAvenant().subscribe(data=>{  
      this.avenants= data;
     console.log(this.avenants);
    });
  }
  public getAllContrat(){
   
    return this.service.search().subscribe(data=>{  
      this.contrats= data
    });
  }

  public updateAvenant(f){
    return this.service.updateAvenant(f).subscribe(data =>{ console.log(data);
      this.succes();
   },error=>{
     this.InvalidFromAvenant=error["error"]["message"]; 
   
   })
  }
  
  delete(id){
    return this.service.deleteAvenant(id).subscribe(data=>{
      location.reload();
    })
  }

  deletePopup(id){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer ce Avenant ",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer avenant avec succes.',
          'success'
        ).then(()=>{
          this.delete(id);
        })
      }
    })
  }
  succes(): void{
    swal.fire(
      'Creation Avenant!',
      'Votre avez creer un nouveau avenant!',
      'success'
    )
    
    }


    private loadScripts() {
      this.dynamicScriptLoader.load('c2').then(data => {
      }).catch(error => console.log(error));
    }
}

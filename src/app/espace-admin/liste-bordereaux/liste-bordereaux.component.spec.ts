import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeBordereauxComponent } from './liste-bordereaux.component';

describe('ListeBordereauxComponent', () => {
  let component: ListeBordereauxComponent;
  let fixture: ComponentFixture<ListeBordereauxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeBordereauxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeBordereauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

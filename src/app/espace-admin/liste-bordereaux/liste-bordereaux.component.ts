import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';


@Component({
  selector: 'app-liste-bordereaux',
  templateUrl: './liste-bordereaux.component.html',
  styleUrls: ['./liste-bordereaux.component.css']
})
export class ListeBordereauxComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit() {
    
     this.loadScripts();
  }

  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('c2').then(data => {
    }).catch(error => console.log(error));
  }
}

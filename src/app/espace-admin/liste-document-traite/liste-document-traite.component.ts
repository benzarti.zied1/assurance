import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FileService } from 'src/app/services/file.service';
import { ParticulierService } from 'src/app/services/particulier.service';
import swal from 'sweetalert2';



@Component({
  selector: 'app-liste-document-traite',
  templateUrl: './liste-document-traite.component.html',
  styleUrls: ['./liste-document-traite.component.css']
})
export class ListeDocumentTraiteComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:FileService,private serviceClient:ParticulierService) { }
  files=[];
  client:any[]=[];
  InvalidFrom;


  ngOnInit() {
    this.getAll();
    this.getAllClient();

    this.loadScripts();
 }


 public getAll(){
   
  return this.service.searchFileTraite().subscribe(data=>{  
    console.log(data);
    this.files=data;
   })
}
public getAllClient(){
  return this.serviceClient.search().subscribe(data=>{
   
  this.client= data["client"].map(el=>{
     return {
       id:el.id,
       nom:el.nom,
       email:el.email,
       adresse:el.adresse,
       tel:el.tel,
       mobile:el.mobile
      
      }
      
   })



  })



}

update(f){
  this.service.update(f)
  .subscribe(data =>{ 
   this.succes();
},error=>{
  this.InvalidFrom=error["error"]["message"]; 

})
}
succes(): void{
  swal.fire(
    'Modification!',
    'Votre avez modifier votre Fichier!',
    'success'
  )
  
  } 


  delete(document){
    return this.service.deleteTraite(document).subscribe(data=>{
      location.reload();
    })
  }
  
  deletePopup(document){
    swal.fire({
      title: 'Vous étes sur?',
      text: "Vous avez supprimer le document "+document,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui!'
    }).then((result) => {
      if (result.value) {
        swal.fire(
          'Suppression!',
          'Vous avez supprimer le document avec succes.',
          'success'
        ).then(()=>{
          this.delete(document);
        })
      }
    })
  }
 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }
}

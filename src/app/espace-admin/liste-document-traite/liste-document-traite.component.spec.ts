import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDocumentTraiteComponent } from './liste-document-traite.component';

describe('ListeDocumentTraiteComponent', () => {
  let component: ListeDocumentTraiteComponent;
  let fixture: ComponentFixture<ListeDocumentTraiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDocumentTraiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDocumentTraiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

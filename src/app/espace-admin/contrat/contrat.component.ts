import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';

@Component({
  selector: 'app-contrat',
  templateUrl: './contrat.component.html',
  styleUrls: ['./contrat.component.css']
})
export class ContratComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit() {
    
    this.loadScripts();
 }

 // js
 private loadScripts() {
   this.dynamicScriptLoader.load('c2').then(data => {
   }).catch(error => console.log(error));
 }

}

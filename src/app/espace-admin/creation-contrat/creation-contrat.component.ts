import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { RisqueService } from 'src/app/services/risque.service';
import { FamilleProduitService } from 'src/app/services/famille-produit.service';
import { Risque } from 'src/app/interfaces/risque';
import { FamilleProduit } from 'src/app/interfaces/famille-produit';
import { ParticulierService } from 'src/app/services/particulier.service';
import { Societe } from 'src/app/interfaces/societe';
import { Particulier } from 'src/app/interfaces/particulier';
import { SocieteService } from 'src/app/services/societe.service';
import swal from 'sweetalert2';
import { Garantie } from 'src/app/interfaces/garantie';
import { GarantieService } from 'src/app/services/garantie.service';
import { CompagnieService } from 'src/app/services/compagnie.service';
import { ApporteurService } from 'src/app/services/apporteur.service';
import { ContratService } from 'src/app/services/contrat.service';
import { exists } from 'fs';
import { Router } from '@angular/router';



@Component({
  selector: 'app-creation-contrat',
  templateUrl: './creation-contrat.component.html',
  styleUrls: ['./creation-contrat.component.css']
})
export class CreationContratComponent implements OnInit {
  selectedValueR1;
  risque:Risque[]=[];
  famille:FamilleProduit[]=[];
  selectedValue:string;
  selectedValueFamille:string;
  client:any[]=[];
  selectedValueClient:string;
  InvalidFromParticulier;
  InvalidFromSociete;
  particulier : any = {
    nomP:'',
    cin:'', 
    adresseP:'',
    emailP:'',
    telP:'',
    mobileP:''
  };

  societe : Societe = {
    nom:'',
    adresse:'',
    email:'',
    tel:'',
    mobile:'',
    raison_sociale:'',
    numero_registre:'', 
    date_creation:null,
    siteWeb:'',
    fax:'',
  };
  selectedValueGarantie:string;
  garantie:Garantie[]=[];
  niveau:any[]=[];
  niveau1:any[]=[];
  selectedValueCompagnie:string;
  selectedValueApporteur:string;
  compagnies:any[]=[];
  apporteur:any []=[];
  selectedValueCarburant;
  selectedValueModeFractionnement="Annuelle";
  selectedValueReconductible="oui";
  selectedValueQuittance;
  selectedValueQuittance1;
  counter = Array;
  counter1 = Array;
  cht:Array<number>=[];
  taxe:Array<number>=[];
  cttc:Array<number>=[];
  prixht=0;
  prixtaxe=0;
  prixttc=0;

  cht1:Array<number>=[];
  taxe1:Array<number>=[];
  cttc1:Array<number>=[];
  prixht1=0;
  prixtaxe1=0;
  prixttc1=0;
  




  constructor(private router:Router,private service:ContratService ,private dynamicScriptLoader: DynamicFileAdminServiceService,private serviceRisque:RisqueService,private serviceFamille:FamilleProduitService,private serviceClient:ParticulierService,private serviceSociete:SocieteService,private serviceGarantie:GarantieService,private serviceApporteur:ApporteurService,private serviceCompagnie:CompagnieService) { }

  ngOnInit() {
    this.getAllRisque();
    this.getAllFamille();
    this.getAllClient();
    this.getAllGarantie();
    this.getAllCompagnie();
    this.getAllApporteur();
     this.loadScripts();
  }
savequittance(f){
  console.log(f);
}

save(f,quittance){
 f.famille=this.selectedValueFamille;
   quittance.totalHt1=this.prixht;
    quittance.totalTaxe1=this.prixtaxe;
    quittance.total1=this.prixttc;
    quittance.totalHt2=this.prixht1;
    quittance.totalTaxe2=this.prixtaxe1;
    quittance.total2=this.prixttc1;
  this.service.add(f,quittance).subscribe(data =>{
    this.succes();
       },
    
    error => {//console.log(error);
    // console.log(error["error"]["message"]); 
  
   });

//console.log(quittance);
  }
  public getAllRisque(){
    return this.serviceRisque.search().subscribe(data=>{
     
    this.risque= data["risque"].map(el=>{
       return {
         id:el.id,
         image:el.image,
         nom:el.nom
        
        }
     })    

    })
  }

  public getAllFamille(){
 
    return this.serviceFamille.search().subscribe(data=>{  
      this.famille= data["familleproduit"].map(el=>{   
    
      console.log("el.risquye == "+el.risqueId);
        return {
         id:el.id,
         image:el.image,
         nom:el.nom,
         risque:el.risqueId   
        }
     })
  

    })
  }

  public getAllClient(){
    return this.serviceClient.search().subscribe(data=>{
     
    this.client= data["client"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })
 
 

    })

  

  }


  saveParticulier(particulier:Particulier) {
    
  
    this.serviceClient.addParticulier(particulier)
     .subscribe(data =>{ console.log(data);
      this.succesClient();
      this.getAllClient();
  
        },
     
     error => {
      this.InvalidFromParticulier=error["error"]["message"]; 
      console.log(error);
    });
  }

  saveSociete(societe:Societe) {
    
  
    this.serviceSociete.addSociete(societe)
     .subscribe(data =>{ console.log(data);
      this.succesClient();
    this.getAllClient();
        },
     
     error => {
      this.InvalidFromSociete=error["error"]["message"]; 

    });
   // console.log(societe);
  }


  succesClient(): void{
    swal.fire(
      'Creation client!',
      'Votre avez créer un nouveau client!',
      'success'
    )
  
  }
succes(): void{
  swal.fire(
    'Creation contrat!',
    'Votre avez créer un nouveau contrat!',
    'success'
  ).then(()=>{
    this.router.navigate(['liste_contrats']);
  })

}

public getAllGarantie(){
   
  return this.serviceGarantie.search().subscribe(data=>{  
    this.garantie= data["garantie"].map(el=>{   
  
      return {
       id:el.id,
       nom:el.nom,
       niveau:el.niveau,
       montantAssure:el.montantAssure,
       montantFranchise:el.montantFranchise,
       familleproduit:el.familleproduitId   
      }
   })
 var ligne:Garantie[]=[];
 ligne.push(this.garantie[0]);
 for(let i=1;i<this.garantie.length;i++){
  
  var fam=this.garantie[i].familleproduit;
   var niv=this.garantie[i].niveau;
   var ok=false;
 
 for(let j=0;j<ligne.length;j++){
  if(niv==ligne[j].niveau && fam==ligne[j].familleproduit){
    ok=true;
  }
 }
if(!ok){
ligne.push(this.garantie[i]);

}
   
  
 
 }
 this.niveau=ligne;
     
     
  })
}

 
deleteRow (index:number) {
  this.garantie.splice(index, 1); //replace your Model here instead of this.user
}

public getAllCompagnie(){
  return this.serviceCompagnie.search().subscribe(data=>{
   
  this.compagnies= data["user"].map(el=>{
     return {
       id:el.id,
       username:el.username,
       email:el.email
       
      
      }
   })

  })
}
public getAllApporteur(){
  return this.serviceApporteur.search().subscribe(data=>{
   
  this.apporteur= data["apporteur"].map(el=>{
     return {
       id:el.id,
       nom:el.nom,
       email:el.email,
       adresse:el.adresse,
       tel:el.tel,
       mobile:el.mobile
      
      }
      
   })



  })



}


calculerttc(){
  for(var i =0 ; i<this.taxe.length;i++){
    var prixttc=0;
    prixttc=this.cht[i]+this.taxe[i];
    this.cttc[i]=prixttc;
  }
  this.calculerprix();
}

calculerprix(){
  this.prixtaxe=0;
  this.prixht=0;
  this.prixttc=0;
  for(var i =0 ; i<this.taxe.length;i++){
    this.prixtaxe=this.prixtaxe+this.taxe[i];
    this.prixht+=this.cht[i];
    
  }
  this.prixttc=this.prixtaxe+this.prixht;
}

calculerttc1(){
  for(var i =0 ; i<this.taxe1.length;i++){
    var prixttc1=0;
    prixttc1=this.cht1[i]+this.taxe1[i];
    this.cttc1[i]=prixttc1;
  }
  this.calculerprix1();
}

calculerprix1(){
  this.prixtaxe1=0;
  this.prixht1=0;
  this.prixttc1=0;
  for(var i =0 ; i<this.taxe1.length;i++){
    this.prixtaxe1=this.prixtaxe1+this.taxe1[i];
    this.prixht1+=this.cht1[i];
    
  }
  this.prixttc1=this.prixtaxe1+this.prixht1;
}
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('s2').then(data => {
    }).catch(error => console.log(error));
  }

}

import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ContratService } from 'src/app/services/contrat.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CompagnieService } from 'src/app/services/compagnie.service';
import { ApporteurService } from 'src/app/services/apporteur.service';
import { Voiture } from 'src/app/interfaces/voiture';
import swal from 'sweetalert2';



@Component({
  selector: 'app-modifier-contrat',
  templateUrl: './modifier-contrat.component.html',
  styleUrls: ['./modifier-contrat.component.css']
})
export class ModifierContratComponent implements OnInit {
voiture={};
contrat={};
selectedValueApporteur;
compagnies;
apporteur;
selectedValueCompagnie;
selectedValueCarburant;
id;
InvalidFrom;

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private service:ContratService,private route:ActivatedRoute,private serviceApporteur:ApporteurService,private serviceCompagnie:CompagnieService,private router:Router) { }

  ngOnInit() {
    
     this.loadScripts();
     this.getAllApporteur();
     this.getAllCompagnie();
     this.getOneVoiture();
     this.getOneContrat();
  }
  public getOneVoiture(){
    this.route.params.subscribe(params => {
      this.id=params['id'];
   });
    return this.service.getOneVoiture(this.id).subscribe(data=>{
  
      this.voiture=data
      
    })
  
    
  }
  public getOneContrat(){
    this.route.params.subscribe(params => {
      this.id=params['id'];
   });
    return this.service.getOneContrat(this.id).subscribe(data=>{
  
      this.contrat=data
      
    })
  
    
  }

  public getAllCompagnie(){
    return this.serviceCompagnie.search().subscribe(data=>{
     
    this.compagnies= data["user"].map(el=>{
       return {
         id:el.id,
         username:el.username,
         email:el.email
         
        
        }
     })
  
    })
  }
  public getAllApporteur(){
    return this.serviceApporteur.search().subscribe(data=>{
     
    this.apporteur= data["apporteur"].map(el=>{
       return {
         id:el.id,
         nom:el.nom,
         email:el.email,
         adresse:el.adresse,
         tel:el.tel,
         mobile:el.mobile
        
        }
        
     })
    })
  }
  public update(f){
    this.route.params.subscribe(params => {
      this.id=params['id'];
   });
   f.id=this.id;
    return this.service.update(f).subscribe(data =>{ console.log(data);
      this.succes();
   },error=>{
     this.InvalidFrom=error["error"]["message"]; 
   
   })
  }
  
  succes(): void{
    swal.fire(
      'Update Contrat!',
      'Votre avez mis a jours un contrat avec succées!',
      'success'
    ).then(()=>{
      this.router.navigate(['/liste_contrats']);
    })
    
    }
  
  // js
  private loadScripts() {
    this.dynamicScriptLoader.load('s2','s7','s8').then(data => {
    }).catch(error => console.log(error));
  }

}

import { TestBed } from '@angular/core/testing';

import { DynamicFileServiceService } from './dynamic-file-service.service';

describe('DynamicFileServiceService', () => {
  let service: DynamicFileServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DynamicFileServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

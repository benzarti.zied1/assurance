import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContratService {
  public host:string="http://localhost:3000/contrat";
  public voitures:string="http://localhost:3000/voitures";
  public contratGarantie:string="http://localhost:3000/contratsGarantie";
  public avenant:string="http://localhost:3000/avenant";


  constructor(private httpClient:HttpClient) { }
  public add(contrat,quittance){
    return this.httpClient.post<any>(this.host,{contrat,quittance});
  }

  public search(){
    return this.httpClient.get<any>(this.host);
  }
  public searchAvenant(){
    return this.httpClient.get<any>(this.avenant);
  }
  public getOneVoiture(id){
    return this.httpClient.get<any>(this.voitures+"/"+id);
  }

  
  public getOneContrat(id){
    return this.httpClient.get<any>(this.host+"/"+id);
  }
  public searchVoiture(){
    return this.httpClient.get<any>(this.voitures);
  }
  public searchContratGarantie(){
    return this.httpClient.get<any>(this.contratGarantie);
  }
  public createAvenant(avenant){
    return this.httpClient.post<any>(this.avenant,avenant);
  }
  public updateAvenant(avenant){
    return this.httpClient.put<any>(this.avenant+"/"+avenant.id,avenant);
  }

  public deleteAvenant(id){
    return this.httpClient.delete<any>(this.avenant+"/"+id);
  }

  public update(f){
    return this.httpClient.put<any>(this.host+"/"+f.id,f);
  }
}

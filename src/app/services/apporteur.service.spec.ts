import { TestBed } from '@angular/core/testing';

import { ApporteurService } from './apporteur.service';

describe('ApporteurService', () => {
  let service: ApporteurService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApporteurService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const AUTHORITIES_KEY ='AuthAuthorities';

@Injectable({
  providedIn: 'root'
})
export class CanActivateService implements CanActivate{

  constructor(private router: Router) { }

  public canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean > | Promise<boolean > | boolean  {
    return new Promise((resolve)=>{
      if(sessionStorage.getItem(TOKEN_KEY))
      this.router.navigate(['/accueil']);
    
    })
    
    
 }
}

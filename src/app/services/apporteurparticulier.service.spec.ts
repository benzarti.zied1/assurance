import { TestBed } from '@angular/core/testing';

import { ApporteurparticulierService } from './apporteurparticulier.service';

describe('ApporteurparticulierService', () => {
  let service: ApporteurparticulierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApporteurparticulierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

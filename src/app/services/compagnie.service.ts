import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Compagnie } from '../interfaces/compagnie';

@Injectable({
  providedIn: 'root'
})
export class CompagnieService {
  public host:string="http://localhost:3000/api/auth/";

  constructor(private httpClient:HttpClient) { }
  public add(compagnie:Compagnie ){
    return this.httpClient.post<Compagnie>(this.host+"signupcompagnie",compagnie);
  }

  public search(){
    return this.httpClient.get<any>(this.host+"getAllCompagnie");
      }

      public delete(id){
        return this.httpClient.delete<void>(this.host+"deleteCompagnie/"+id);
      }

      public update(compagnie){
        return this.httpClient.put<Compagnie>(this.host+"updateCompagnie/"+compagnie.id,compagnie);
      }
}

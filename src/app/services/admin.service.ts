import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Admin  } from '../interfaces/admin';
import { Observable } from 'rxjs';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenStorageService } from './token-storage.service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const AUTHORITIES_KEY ='AuthAuthorities';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AdminService implements CanActivate{

  public host:string="http://localhost:3000/api/auth/";

  constructor(private tokenStorage: TokenStorageService,private httpClient:HttpClient,private router:Router) { }
  roles=this.tokenStorage.getUser().roles[0];

  public add(admin:Admin ){
    return this.httpClient.post<Admin>(this.host+"signupadmin",admin);
  }
  login(credentials): Observable<any> {
    return this.httpClient.post(this.host+"/signin" , {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }
  public canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean > | Promise<boolean > | boolean  {
    return new Promise((resolve)=>{
     // for(let role in this.roles) 
      if(this.roles === 'ROLE_ADMIN')
     resolve(true)
     else{
       this.router.navigate(['/']);
     }
    })
    
    
 }
 public search():Observable<any>{
  return this.httpClient.get<any>(this.host+"getAllAdmin");
    }
    
    public delete(id):Observable<void>{
      return this.httpClient.delete<void>(this.host+"deleteAdmin/"+id);
    }

    public update(admin):Observable<Admin>{
      return this.httpClient.put<Admin>(this.host+"updateAdmin/"+admin.id,admin);
    }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Agence  } from '../interfaces/agence';
import { Observable } from 'rxjs';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenStorageService } from './token-storage.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AgenceService implements CanActivate{
  public host:string="http://localhost:3000/api/auth/";
 

  constructor(private tokenStorage: TokenStorageService,private httpClient:HttpClient,private router:Router) { }
  roles=this.tokenStorage.getUser().roles[0];

  public add(agence:Agence ){
    return this.httpClient.post<Agence>(this.host+"signupagence",agence);
  }
  login(credentials): Observable<any> {
    return this.httpClient.post(this.host+"signin" , {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }
  public canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean > | Promise<boolean > | boolean  {
    return new Promise((resolve)=>{
      if(this.roles === 'ROLE_AGENCE')
     resolve(true)
     else{
       this.router.navigate(['/']);
     }
    })
    
    
 }
 public search():Observable<any>{
  return this.httpClient.get<any>(this.host+"getAllAgence");
    }
    
    public delete(id):Observable<void>{
      return this.httpClient.delete<void>(this.host+"deleteAgence/"+id);
    }

    public update(agence):Observable<Agence>{
      return this.httpClient.put<Agence>(this.host+"updateAgence/"+agence.id,agence);
    }
}

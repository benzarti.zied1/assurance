import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Societe } from '../interfaces/societe';

@Injectable({
  providedIn: 'root'
})
export class SocieteService {

  public host:string="http://localhost:3000/api/auth/";


  constructor(private httpClient:HttpClient) { }
  public addSociete(societe:Societe ){
    return this.httpClient.post<Societe>(this.host+"create_societe",societe);
  }
}

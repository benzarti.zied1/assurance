import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  public host:string="http://localhost:3000/file";
  public hostTraite:string="http://localhost:3000/fileTraite";


  constructor(private httpClient:HttpClient) { }
  public add(file){
    return this.httpClient.post<any>(this.host,file);
  }
  public search(){
    return this.httpClient.get<any>(this.host);
  }

  public delete(document){
    return this.httpClient.delete<any>(this.host+"/"+document);
  }
  public searchFileTraite(){
    return this.httpClient.get<any>(this.hostTraite);
  }

  public update(file){
    return this.httpClient.put<any>(this.hostTraite+"/"+file.id,file);
  }

  public deleteTraite(document){
    return this.httpClient.delete<any>(this.hostTraite+"/"+document);
  }

}

import { Injectable } from '@angular/core';
import { Particulier } from '../interfaces/particulier';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Societe } from '../interfaces/societe';


@Injectable({
  providedIn: 'root'
})
export class ParticulierService {
  public host:string="http://localhost:3000/api/auth/";



  constructor(private httpClient:HttpClient) { }
  public addParticulier(particuler:Particulier ){
    return this.httpClient.post<Particulier>(this.host+"create_particulier",particuler);
  }

  public search(){
    return this.httpClient.get<any>(this.host+"getAllClient");
      }

      public getOneParticulier(id){
        return this.httpClient.get<any>(this.host+"getOneParticulier/"+id);
          }

          
      public getOneSociete(id){
        return this.httpClient.get<any>(this.host+"getOneSociete/"+id);
          }

          public checkClient(id){
            return this.httpClient.get<any>(this.host+"checkClient/"+id);
              }

              public searchParticulier(){
                return this.httpClient.get<any>(this.host+"getAllParticulier");
                  }

                  public searchSociete(){
                    return this.httpClient.get<any>(this.host+"getAllSociete");
                      }


                      public updateParticulier(p):Observable<Particulier>{
                        return this.httpClient.put<Particulier>(this.host+"updateParticulier/"+p.id,p);
                      }

                      public updateSociete(s):Observable<Societe>{
                        return this.httpClient.put<Societe>(this.host+"updateSociete/"+s.id,s);
                      }

                      public deleteParticulier(id):Observable<void>{
                        return this.httpClient.delete<void>(this.host+"deleteParticulier/"+id);
                      }

                      public deleteSociete(id):Observable<void>{
                        return this.httpClient.delete<void>(this.host+"deleteSociete/"+id);
                      }
}

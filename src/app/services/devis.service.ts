import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Voiture } from '../interfaces/voiture';

@Injectable({
  providedIn: 'root'
})
export class DevisService {

  public host:string="http://localhost:3000/devis";
  public voitures:string="http://localhost:3000/voitures";
  public devisGarantie:string="http://localhost:3000/devisGarantie";

  constructor(private httpClient:HttpClient) { }

  public add(devis){
    return this.httpClient.post<any>(this.host,devis);
  }

  public search(){
    return this.httpClient.get<any>(this.host);
  }

  public searchVoiture(){
    return this.httpClient.get<any>(this.voitures);
  }
  public searchDevisGarantie(){
    return this.httpClient.get<any>(this.devisGarantie);
  }

  public update(voiture){
    return this.httpClient.put<any>(this.host+"/"+voiture.id,voiture);
  }

  public delete(id){
    return this.httpClient.delete<any>(this.host+"/"+id);
  }
}

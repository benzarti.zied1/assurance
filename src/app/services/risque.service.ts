import { Injectable } from '@angular/core';
import { Risque } from '../interfaces/risque';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RisqueService {
  public host:string="http://localhost:3000/Risque";

  constructor(private httpClient:HttpClient) { }

  public add(risque:Risque ){
    return this.httpClient.post<Risque>(this.host,risque);
  }
  public search():Observable<any>{
    return this.httpClient.get<any>(this.host);
      }

      public delete(id):Observable<void>{
        return this.httpClient.delete<void>(this.host+"/"+id);
      }

      public update(risque):Observable<Risque>{
        return this.httpClient.put<Risque>(this.host+"/"+risque.id,risque);
      }
      public getOne(id):Observable<any>{
        return this.httpClient.get<any>(this.host+"/"+id);
          }
}

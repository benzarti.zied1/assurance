import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Garantie } from '../interfaces/garantie';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GarantieService {

  public host:string="http://localhost:3000/Garantie";

  constructor(private httpClient:HttpClient) { }

  public add(garantie:Garantie ){
    return this.httpClient.post<Garantie>(this.host,garantie);
  }
  public search():Observable<any>{
    return this.httpClient.get<any>(this.host);
      }

      public delete(id):Observable<void>{
        return this.httpClient.delete<void>(this.host+"/"+id);
      }

      public update(garantie):Observable<Garantie>{
        return this.httpClient.put<Garantie>(this.host+"/"+garantie.id,garantie);
      }
}

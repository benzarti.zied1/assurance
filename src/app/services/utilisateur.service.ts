import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Utilisateur  } from '../interfaces/utilisateur';
import { Observable } from 'rxjs';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenStorageService } from './token-storage.service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const AUTHORITIES_KEY ='AuthAuthorities';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class UtilisateurService implements CanActivate{
  public host:string="http://localhost:3000/api/auth/";



  constructor(private tokenStorage: TokenStorageService,private httpClient:HttpClient,private router:Router) { }
  roles=this.tokenStorage.getUser().roles[0];

  public add(utilisateur:Utilisateur ){
    return this.httpClient.post<Utilisateur>(this.host+"signuputilisateur",utilisateur);
  }
  login(credentials): Observable<any> {
    return this.httpClient.post(this.host+"signin", {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  public canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean > | Promise<boolean > | boolean  {
    return new Promise((resolve)=>{
     // for(let role in this.roles) 
      if(this.roles === 'ROLE_UTILISATEUR')
     resolve(true)
     else{
       this.router.navigate(['/']);
     }
    })
    
    
 }

 public search():Observable<any>{
  return this.httpClient.get<any>(this.host+"getAllUtilisateur");
    }
  

    public delete(id):Observable<void>{
      return this.httpClient.delete<void>(this.host+"deleteUtilisateur/"+id);
    }

    public update(utilisateur):Observable<Utilisateur>{
      return this.httpClient.put<Utilisateur>(this.host+"updateUtilisateur/"+utilisateur.id,utilisateur);
    }

    public getOneUtilisateur(id):Observable<Utilisateur>{
      return this.httpClient.get<Utilisateur>(this.host+"getOneUtilisateur/"+id);
    }

    public getOneAgence(id):Observable<Utilisateur>{
      return this.httpClient.get<Utilisateur>(this.host+"getOneAgence"+id);
    }

    public updateOne(utilisateur,id):Observable<Utilisateur>{
      return this.httpClient.put<Utilisateur>(this.host+"updateProfilUtilisateur/"+id,utilisateur);
    }

    public updateOneAgence(utilisateur,id):Observable<Utilisateur>{
      return this.httpClient.put<Utilisateur>(this.host+"updateProfilAgence/"+id,utilisateur);
    }
    public changePasswordUtilisateur(f,email):Observable<Utilisateur>{
      return this.httpClient.put<Utilisateur>(this.host+"changePasswordProfil/"+email,f);

    }

    public getOneAdmin(id):Observable<Utilisateur>{
      return this.httpClient.get<Utilisateur>(this.host+"getOneAdmin/"+id);
    }

}

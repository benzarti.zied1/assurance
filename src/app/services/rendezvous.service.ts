import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Rendezvous } from '../interfaces/rendezvous';

@Injectable({
  providedIn: 'root'
})
export class RendezvousService {
  public host:string="http://localhost:3000/rendez-vous";

  constructor(private httpClient:HttpClient) { }

  public add(rendezvous){
    return this.httpClient.post<Rendezvous>(this.host,rendezvous);
  }

  public search(){
    return this.httpClient.get<Rendezvous>(this.host);
  }

  public update(rendezvous){
    return this.httpClient.put<Rendezvous>(this.host+"/"+rendezvous.id,rendezvous);
  }

  public delete(id){
    return this.httpClient.delete<Rendezvous>(this.host+"/"+id);
  }
  

}

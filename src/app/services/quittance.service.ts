import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuittanceService {
  public host:string="http://localhost:3000/quittance";
  public facture:string="http://localhost:3000/facture";
  public ligneFacture:string="http://localhost:3000/ligneFacture";

  constructor(private httpClient:HttpClient) { }
  public createQuittance(quittance){
    return this.httpClient.post<any>(this.host,quittance);
  }
  public search(){
    return this.httpClient.get<any>(this.host);
  }
  public searchFacture(){
    return this.httpClient.get<any>(this.facture);
  }
  public searchLigneFacture(){
    return this.httpClient.get<any>(this.ligneFacture);
  }
}

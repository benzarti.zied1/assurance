import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigurationDevis } from '../interfaces/configuration-devis';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationDevisService {

  public host:string="http://localhost:3000/ConfigurationDevis";

  constructor(private httpClient:HttpClient) { }

  public add(configuration:ConfigurationDevis ){
    return this.httpClient.post<ConfigurationDevis>(this.host,configuration);
  }
  public search():Observable<any>{
    return this.httpClient.get<any>(this.host);
      }

      public delete(id):Observable<void>{
        return this.httpClient.delete<void>(this.host+"/"+id);
      }

      public update(configuration):Observable<ConfigurationDevis>{
        return this.httpClient.put<ConfigurationDevis>(this.host+"/"+configuration.id,configuration);
      }
}

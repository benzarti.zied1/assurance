import { TestBed } from '@angular/core/testing';

import { ConfigurationDevisService } from './configuration-devis.service';

describe('ConfigurationDevisService', () => {
  let service: ConfigurationDevisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigurationDevisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

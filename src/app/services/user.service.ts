import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User  } from '../interfaces/user';
import { Router } from '@angular/router';
import { TokenStorageService } from './token-storage.service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const AUTHORITIES_KEY ='AuthAuthorities';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService{
  public host:string="http://localhost:3000/api/auth/";
  constructor(private tokenStorage: TokenStorageService,private httpClient:HttpClient,private router:Router) { }

  public add(user:User ){
    return this.httpClient.post<User>(this.host+"signupa",user);
  }
  login(credentials): Observable<any> {
    return this.httpClient.post(this.host+"signin", {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }
 
  getCode(email):Observable<any>{
    return this.httpClient.get<any>(this.host+"getCode/"+email);
  }

  changePassword(utilisateur,email):Observable<any>{
    return this.httpClient.put<any>(this.host+"changePassword/"+email,utilisateur);
  }

}


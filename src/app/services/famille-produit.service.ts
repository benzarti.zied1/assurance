import { Injectable } from '@angular/core';
import { FamilleProduit } from '../interfaces/famille-produit';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FamilleProduitService {

  public host:string="http://localhost:3000/FamilleProduit";

  constructor(private httpClient:HttpClient) { }

  public add(familleProduit:FamilleProduit ){
    return this.httpClient.post<FamilleProduit>(this.host,familleProduit);
  }
  public search():Observable<any>{
    return this.httpClient.get<any>(this.host);
      }

      public delete(id):Observable<void>{
        return this.httpClient.delete<void>(this.host+"/"+id);
      }

      public update(familleProduit):Observable<FamilleProduit>{
        return this.httpClient.put<FamilleProduit>(this.host+"/"+familleProduit.id,familleProduit);
      }
}

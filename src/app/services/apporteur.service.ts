import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Particulier } from '../interfaces/particulier';
import { Societe } from '../interfaces/societe';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApporteurService {
  public host:string="http://localhost:3000/api/auth/";
 

  constructor(private httpClient:HttpClient) { }

  public addParticulier(particuler:Particulier ){
    return this.httpClient.post<Particulier>(this.host+"createApporteurParticulier",particuler);
  }

  public addSociete(societe:Societe ){
    return this.httpClient.post<Societe>(this.host+"createApporteurSociete",societe);
  }



  public search(){
    return this.httpClient.get<any>(this.host+"getAllApporteur");
      }

  public searchParticulier(){
    return this.httpClient.get<any>(this.host+"getAllApporteurParticulier");
      }

      public searchSociete(){
        return this.httpClient.get<any>(this.host+"getAllApporteurSociete");
          }


          public updateParticulier(p):Observable<Particulier>{
            return this.httpClient.put<Particulier>(this.host+"updateApporteurParticulier/"+p.id,p);
          }

          public updateSociete(s):Observable<Societe>{
            return this.httpClient.put<Societe>(this.host+"updateApporteurSociete/"+s.id,s);
          }

          public deleteParticulier(id):Observable<void>{
            return this.httpClient.delete<void>(this.host+"deleteApporteurParticulier/"+id);
          }

          public deleteSociete(id):Observable<void>{
            return this.httpClient.delete<void>(this.host+"deleteApporteurSociete/"+id);
          }
}

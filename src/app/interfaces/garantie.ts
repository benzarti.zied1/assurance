export interface Garantie {
    nom:string,
    montantAssure:string,
    montantFranchise:string,
    niveau:string,
    familleproduit:string
}

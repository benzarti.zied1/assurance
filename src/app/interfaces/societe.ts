export interface Societe {
    nom:string,
    adresse:string,
    email:string,
    tel:string,
    mobile:string,
    raison_sociale:string,
    numero_registre:string, 
    date_creation:Date,
    siteWeb:string,
    fax:string,
}

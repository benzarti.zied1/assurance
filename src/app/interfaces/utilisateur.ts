export interface Utilisateur {

    email:string,
    username:string,
    password:string,
    nom:string,
    prenom:string,
    cin:string,
    tel:string,
    autreTel:string,
    codePostal:string,
    date_naissance:Date,
    ville:string,
    role:string

}

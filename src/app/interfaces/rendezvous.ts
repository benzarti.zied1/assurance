export interface Rendezvous {
    id:string;
    date:Date;
    lieu:string;
    description:string;
    client:string;
}

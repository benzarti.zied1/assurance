export interface Particulier {  
    nom:string,
    cin:string, 
    adresse:string,
    email:string,
    tel:string,
    mobile:string
}

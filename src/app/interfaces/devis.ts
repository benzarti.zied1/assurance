export interface Devis {
    id:string 
    numeroPolice:string,
    numeroPoliceExterne: string,
    dateEffet:Date,
    dateFin: Date,
    echeancePrincipale:string,
    modeFractionnement: string,
    delaiPreavis:string,
    montantAgence: string,
    montantTTC:string,
    risque:string,
    famille:string,
    client:string,
    compagnie:string,
    apporteur:string

}

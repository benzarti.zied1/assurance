export interface Voiture {
        id:string,
        anneeFab:string,
        modele: string,
        immatriculation:string,
        dateAcquisition: string,
        adresse:string,
        marque: string,
        trims:string,
        carburant: string,
        dateMiseEnCirculation:string,
        codePostal: string,
}

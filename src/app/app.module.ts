import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AngularFileViewerModule } from '@taldor-ltd/angular-file-viewer';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './espace-admin/navbar/navbar.component';
import { SidebarComponent } from './espace-admin/sidebar/sidebar.component';
import { FooterComponent } from './espace-admin/footer/footer.component';
import { ContentComponent } from './espace-admin/content/content.component';
import { CreationDevisComponent } from './espace-admin/creation-devis/creation-devis.component';
import {Routes,RouterModule} from '@angular/router';
import { ListeDevisComponent } from './espace-admin/liste-devis/liste-devis.component';
import { ListeContratsComponent } from './espace-admin/liste-contrats/liste-contrats.component';
import { CreationContratComponent } from './espace-admin/creation-contrat/creation-contrat.component';
import { CreationQuittanceComponent } from './espace-admin/creation-quittance/creation-quittance.component';
import { ListeQuittancesComponent } from './espace-admin/liste-quittances/liste-quittances.component';
import { CreationCompagnieComponent } from './espace-admin/creation-compagnie/creation-compagnie.component';
import { ListeCompagniesComponent } from './espace-admin/liste-compagnies/liste-compagnies.component';
import { CreationSinistresComponent } from './espace-admin/creation-sinistres/creation-sinistres.component';
import { ListeSinistresComponent } from './espace-admin/liste-sinistres/liste-sinistres.component';
import { CreationClientComponent } from './espace-admin/creation-client/creation-client.component';
import { ListeClientsComponent } from './espace-admin/liste-clients/liste-clients.component';
import { CreationApporteurComponent } from './espace-admin/creation-apporteur/creation-apporteur.component';
import { ListeApporteursComponent } from './espace-admin/liste-apporteurs/liste-apporteurs.component';
import { CreateReclamationComponent } from './espace-admin/create-reclamation/create-reclamation.component';
import { ListeReclamationsComponent } from './espace-admin/liste-reclamations/liste-reclamations.component';
import { CreateReglementComponent } from './espace-admin/create-reglement/create-reglement.component';
import { ListeReglementsComponent } from './espace-admin/liste-reglements/liste-reglements.component';
import { CreateBordereauComponent } from './espace-admin/create-bordereau/create-bordereau.component';
import { ListeBordereauxComponent } from './espace-admin/liste-bordereaux/liste-bordereaux.component';
import { SinistreComponent } from './espace-admin/sinistre/sinistre.component';
import { ConfigurationComponent } from './espace-admin/configuration/configuration.component';
import { ComptabiliteComponent } from './espace-admin/comptabilite/comptabilite.component';
import { DevisComponent } from './espace-admin/devis/devis.component';
import { ContratComponent } from './espace-admin/contrat/contrat.component';
import { GedComponent } from './espace-admin/ged/ged.component';
import { ProfilComponent } from './espace-admin/profil/profil.component';
import { ConnexionComponent } from './espace-admin/connexion/connexion.component';
import { CreationRendezVousComponent } from './espace-admin/creation-rendez-vous/creation-rendez-vous.component';
import { ListeRendezVousComponent } from './espace-admin/liste-rendez-vous/liste-rendez-vous.component';
import { ListeSouhaitComponent } from './espace-admin/liste-souhait/liste-souhait.component';
import { CreationUtilisateurComponent } from './espace-admin/creation-utilisateur/creation-utilisateur.component';
import { ListeUtilisateurComponent } from './espace-admin/liste-utilisateur/liste-utilisateur.component';
import { ListeDocumentNonTraiteComponent } from './espace-admin/liste-document-non-traite/liste-document-non-traite.component';
import { ListeDocumentTraiteComponent } from './espace-admin/liste-document-traite/liste-document-traite.component';
import { DynamicFileAdminServiceService } from './dynamic-file-service.service';
import { ListeGarantieComponent } from './espace-admin/liste-garantie/liste-garantie.component';
import { ListeRisqueComponent } from './espace-admin/liste-risque/liste-risque.component';
import { ListeFamilleProduitComponent } from './espace-admin/liste-famille-produit/liste-famille-produit.component';
import { ListeConfigurationDevisComponent } from './espace-admin/liste-configuration-devis/liste-configuration-devis.component';
import { CreationConfirationDevisguComponent } from './espace-admin/creation-confiration-devisgu/creation-confiration-devisgu.component';
import { CreationFamilleProduitComponent } from './espace-admin/creation-famille-produit/creation-famille-produit.component';
import { CreationConfirationDeComponent } from './espace-admin/creation-confiration-de/creation-confiration-de.component';
import { CreationRisqueComponent } from './espace-admin/creation-risque/creation-risque.component';
import { CreationGarantieComponent } from './espace-admin/creation-garantie/creation-garantie.component';
import { ModifierDevisComponent } from './espace-admin/modifier-devis/modifier-devis.component';
import { ModifierContratComponent } from './espace-admin/modifier-contrat/modifier-contrat.component';
import { ModifierSinistreComponent } from './espace-admin/modifier-sinistre/modifier-sinistre.component';
import { ModifierDevissouhaitComponent } from './espace-admin/modifier-devissouhait/modifier-devissouhait.component';
import { CreationAgenceComponent } from './espace-admin/creation-agence/creation-agence.component';
import { ListeAgenceComponent } from './espace-admin/liste-agence/liste-agence.component';
import { AgenceService } from './services/agence.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { TokenStorageService } from './services/token-storage.service';
import { CanActivateService } from './services/can-activate.service';
import { UtilisateurService } from './services/utilisateur.service';
import { AdminService } from './services/admin.service';
import { ForgotPasswordComponent } from './espace-admin/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './espace-admin/reset-password/reset-password.component';
import { MenuComponent } from './espace-admin/menu/menu.component';
import { CreationAvenantComponent } from './espace-admin/creation-avenant/creation-avenant.component';
import { ListeAvenantComponent } from './espace-admin/liste-avenant/liste-avenant.component';
const routes:Routes=[
  
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    ContentComponent,
    CreationDevisComponent,
    ListeDevisComponent,
    ListeContratsComponent,
    CreationContratComponent,
    CreationQuittanceComponent,
    ListeQuittancesComponent,
    CreationCompagnieComponent,
    ListeCompagniesComponent,
    CreationSinistresComponent,
    ListeSinistresComponent,
    CreationClientComponent,
    ListeClientsComponent,
    CreationApporteurComponent,
    ListeApporteursComponent,
    CreateReclamationComponent,
    CreateReglementComponent,
    ListeReglementsComponent,
    CreateBordereauComponent,
    ListeBordereauxComponent,
    SinistreComponent,
    ConfigurationComponent,
    ComptabiliteComponent,
    DevisComponent,
    ContratComponent,
    GedComponent,
    ProfilComponent,
    ConnexionComponent,
    CreationRendezVousComponent,
    ListeRendezVousComponent,
    ListeSouhaitComponent,
    CreationUtilisateurComponent,
    ListeUtilisateurComponent,
    ListeDocumentNonTraiteComponent,
    ListeDocumentTraiteComponent,
    ListeGarantieComponent,
    ListeRisqueComponent,
    ListeFamilleProduitComponent,
    ListeConfigurationDevisComponent,
    CreationConfirationDevisguComponent,
    CreationFamilleProduitComponent,
    CreationConfirationDeComponent,
    CreationRisqueComponent,
    CreationGarantieComponent,
    ModifierDevisComponent,
    ModifierContratComponent,
    ModifierSinistreComponent,
    ModifierDevissouhaitComponent,
    CreationAgenceComponent,
    ListeAgenceComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    MenuComponent,
    CreationAvenantComponent,
    ListeAvenantComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFileViewerModule
  ],
  providers: [DynamicFileAdminServiceService,
  AdminService,
  AgenceService,
  UtilisateurService,
  AuthInterceptorService,
  TokenStorageService,
  CanActivateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
